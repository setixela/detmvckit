//
//  TestTCS.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 29.12.2017.
//  Copyright © 2017 Gaozhiyuan. All rights reserved.
//

import Foundation
import UIKit

protocol CellData {
   associatedtype D
}

final class PlainCell: BaseCell, Prototype, CellData {

   typealias D = JTest
   static var prototype: PlainCell = PlainCell()
   
   override class func kHeight() -> CGFloat {
      return 50
   }
   
   public override func bind(_ model: Contentable) -> Self {
      if let jtest = model as? D {
         self.textLabel?.text = jtest.string(.id)
      }
      
      print("Override bind!")
      return self
   }
}

enum SEARCH_CELLS: EnumCells {
   
   case
   plainCell
   
   public var prototype: BaseCell {
      switch self {
      case .plainCell:
         return PlainCell.prototype
      }
   }
}

class JTest: JModel, JSONModelProtocol, CellTypable {

   var cellType: EnumCells!
   
   enum Fields: String {
      case
         id
   }
}

final class TestTCS: BaseTCS<SEARCH_CELLS> {
   override var searchBarHeight: CGFloat { return 48 }
   
   override func setupSearchController() {
      self.setupSearchController()
   }
}
