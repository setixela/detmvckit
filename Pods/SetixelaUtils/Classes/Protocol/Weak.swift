//
//  Weak.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 07.06.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public class Weak<T: AnyObject> {
   weak var value: T?
   public init(value: T) {
      self.value = value
   }
}

public extension Array where Element: Weak<AnyObject> {
   mutating func reap() {
      self = self.filter { nil != $0.value }
   }
}
