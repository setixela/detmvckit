//
//  Singleton.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 11.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

public protocol Singleton {
   static var shared: Self { get }
}
