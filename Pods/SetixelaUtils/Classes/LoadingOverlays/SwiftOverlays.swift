//
//  SwiftOverlay.swift
//
//  Created by AlexSolo on 14.06.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import UIKit

open class SwiftOverlays: NSObject {
   // You can customize these values
   
   static let backAlpha = 0.65 // wait back darkness
   
   // Some random number
   static let containerViewTag = 456987123
   
   static let cornerRadius = CGFloat(10)
   static let padding = CGFloat(10)
   
   static let backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.35)
   static let textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
   static let font = UIFont.systemFont(ofSize: 14)
   
   // Annoying notifications on top of status bar
   static let bannerDissapearAnimationDuration = 0.5
   
   static var bannerWindow: UIWindow?
   
   open class Utils {
      public static func centerViewInSuperview(_ view: UIView) {
         assert(view.superview != nil, "`view` should have a superview")
         
         view.translatesAutoresizingMaskIntoConstraints = false
         
         let constraintH = NSLayoutConstraint(item: view,
                                              attribute: NSLayoutConstraint.Attribute.centerX,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: view.superview,
                                              attribute: NSLayoutConstraint.Attribute.centerX,
                                              multiplier: 1,
                                              constant: 0)
         let constraintV = NSLayoutConstraint(item: view,
                                              attribute: NSLayoutConstraint.Attribute.centerY,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: view.superview,
                                              attribute: NSLayoutConstraint.Attribute.centerY,
                                              multiplier: 1,
                                              constant: 0)
         let constraintWidth = NSLayoutConstraint(item: view,
                                                  attribute: NSLayoutConstraint.Attribute.width,
                                                  relatedBy: NSLayoutConstraint.Relation.equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                                  multiplier: 1,
                                                  constant: view.frame.size.width)
         let constraintHeight = NSLayoutConstraint(item: view,
                                                   attribute: NSLayoutConstraint.Attribute.height,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: view.frame.size.height)
         view.superview!.addConstraints([constraintV, constraintH, constraintWidth, constraintHeight])
      }
   }
   
   // MARK: - Public class methods -
   
   // MARK: Blocking
   
   @discardableResult
   open class func showBlockingWaitOverlay() -> UIView {
      let blocker = addMainWindowBlocker()
      showCenteredWaitOverlay(blocker)
      
      return blocker
   }
   
   @discardableResult
   open class func showBlockingWaitOverlayWithText(_ text: String) -> UIView {
      let blocker = addMainWindowBlocker()
      showCenteredWaitOverlayWithText(blocker, text: text)
      
      return blocker
   }
   
   open class func showBlockingImageAndTextOverlay(_ image: UIImage, text: String) -> UIView {
      let blocker = addMainWindowBlocker()
      showImageAndTextOverlay(blocker, image: image, text: text)
      
      return blocker
   }
   
   open class func showBlockingTextOverlay(_ text: String) -> UIView {
      let blocker = addMainWindowBlocker()
      showTextOverlay(blocker, text: text)
      
      return blocker
   }
   
   open class func removeAllBlockingOverlays() {
      let window = UIApplication.shared.delegate!.window!!
      removeAllOverlaysFromView(window)
   }
   
   // MARK: Non-blocking
   
   @discardableResult
   open class func showCenteredWaitOverlay(_ parentView: UIView) -> UIView {
      let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
      ai.startAnimating()
      
      let containerViewRect = CGRect(x: 0,
                                     y: 0,
                                     width: ai.frame.size.width * 2,
                                     height: ai.frame.size.height * 2)
      
      let containerView = UIView(frame: containerViewRect)
      
      containerView.tag = containerViewTag
      containerView.layer.cornerRadius = cornerRadius
      containerView.backgroundColor = backgroundColor
      containerView.center = CGPoint(x: parentView.bounds.size.width / 2,
                                     y: parentView.bounds.size.height / 2)
      
      ai.center = CGPoint(x: containerView.bounds.size.width / 2,
                          y: containerView.bounds.size.height / 2)
      
      containerView.addSubview(ai)
      
      parentView.addSubview(containerView)
      
      Utils.centerViewInSuperview(containerView)
      
      return containerView
   }
   
   @discardableResult
   open class func showCenteredWaitOverlayWithText(_ parentView: UIView, text: String) -> UIView {
      let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
      ai.startAnimating()
      
      return showGenericOverlay(parentView, text: text, accessoryView: ai)
   }
   
   @discardableResult
   open class func showImageAndTextOverlay(_ parentView: UIView, image: UIImage, text: String) -> UIView {
      let imageView = UIImageView(image: image)
      
      return showGenericOverlay(parentView, text: text, accessoryView: imageView)
   }
   
   open class func showGenericOverlay(_ parentView: UIView, text: String, accessoryView: UIView, horizontalLayout: Bool = true) -> UIView {
      let label = labelForText(text)
      var actualSize = CGSize.zero
      
      if horizontalLayout {
         actualSize = CGSize(width: accessoryView.frame.size.width + label.frame.size.width + padding * 3,
                             height: max(label.frame.size.height, accessoryView.frame.size.height) + padding * 2)
         
         label.frame = label.frame.offsetBy(dx: accessoryView.frame.size.width + padding * 2, dy: padding)
         
         accessoryView.frame = accessoryView.frame.offsetBy(dx: padding, dy: (actualSize.height - accessoryView.frame.size.height) / 2)
      } else {
         actualSize = CGSize(width: max(accessoryView.frame.size.width, label.frame.size.width) + padding * 2,
                             height: label.frame.size.height + accessoryView.frame.size.height + padding * 3)
         
         label.frame = label.frame.offsetBy(dx: padding, dy: accessoryView.frame.size.height + padding * 2)
         
         accessoryView.frame = accessoryView.frame.offsetBy(dx: (actualSize.width - accessoryView.frame.size.width) / 2, dy: padding)
      }
      
      // Container view
      let containerViewRect = CGRect(x: 0,
                                     y: 0,
                                     width: actualSize.width,
                                     height: actualSize.height)
      
      let containerView = UIView(frame: containerViewRect)
      
      containerView.tag = containerViewTag
      containerView.layer.cornerRadius = cornerRadius
      containerView.backgroundColor = backgroundColor
      containerView.center = CGPoint(x: parentView.bounds.size.width / 2,
                                     y: parentView.bounds.size.height / 2)
      
      containerView.addSubview(accessoryView)
      containerView.addSubview(label)
      
      parentView.addSubview(containerView)
      
      Utils.centerViewInSuperview(containerView)
      
      return containerView
   }
   
   @discardableResult
   open class func showTextOverlay(_ parentView: UIView, text: String) -> UIView {
      let label = labelForText(text)
      label.frame = label.frame.offsetBy(dx: padding, dy: padding)
      
      let actualSize = CGSize(width: label.frame.size.width + padding * 2,
                              height: label.frame.size.height + padding * 2)
      
      // Container view
      let containerViewRect = CGRect(x: 0,
                                     y: 0,
                                     width: actualSize.width,
                                     height: actualSize.height)
      
      let containerView = UIView(frame: containerViewRect)
      
      containerView.tag = containerViewTag
      containerView.layer.cornerRadius = cornerRadius
      containerView.backgroundColor = backgroundColor
      containerView.center = CGPoint(x: parentView.bounds.size.width / 2,
                                     y: parentView.bounds.size.height / 2)
      
      containerView.addSubview(label)
      
      parentView.addSubview(containerView)
      
      Utils.centerViewInSuperview(containerView)
      
      return containerView
   }
   
   open class func showProgressOverlay(_ parentView: UIView, text: String) -> UIView {
      let pv = UIProgressView(progressViewStyle: .default)
      
      return showGenericOverlay(parentView, text: text, accessoryView: pv, horizontalLayout: false)
   }
   
   open class func removeAllOverlaysFromView(_ parentView: UIView) {
      var overlay: UIView?
      
      while true {
         overlay = parentView.viewWithTag(containerViewTag)
         if overlay == nil {
            break
         }
         
         overlay!.removeFromSuperview()
      }
   }
   
   open class func updateOverlayText(_ parentView: UIView, text: String) {
      if let overlay = parentView.viewWithTag(containerViewTag) {
         for subview in overlay.subviews {
            if let label = subview as? UILabel {
               label.text = text as String
               break
            }
         }
      }
   }
   
   open class func updateOverlayProgress(_ parentView: UIView, progress: Float) {
      if let overlay = parentView.viewWithTag(containerViewTag) {
         for subview in overlay.subviews {
            if let pv = subview as? UIProgressView {
               pv.progress = progress
               break
            }
         }
      }
   }
}

extension SwiftOverlays {

   // MARK: Status bar notification
   
   open class func showAnnoyingNotificationOnTopOfStatusBar(_ notificationView: UIView, duration: TimeInterval, animated: Bool = true) {
      if bannerWindow == nil {
         bannerWindow = UIWindow()
         bannerWindow!.windowLevel = UIWindow.Level.statusBar + 1
         bannerWindow!.backgroundColor = UIColor.clear
      }
      
      bannerWindow!.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: notificationView.frame.size.height)
      bannerWindow!.isHidden = false
      
      let selector = #selector(closeAnnoyingNotificationOnTopOfStatusBar)
      let gestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
      notificationView.addGestureRecognizer(gestureRecognizer)
      
      bannerWindow!.addSubview(notificationView)
      
      if animated {
         let frame = notificationView.frame
         let origin = CGPoint(x: 0, y: -frame.height)
         notificationView.frame = CGRect(origin: origin, size: frame.size)
         
         // Show appearing animation, schedule calling closing selector after completed
         UIView.animate(withDuration: bannerDissapearAnimationDuration, animations: {
            let frame = notificationView.frame
            notificationView.frame = frame.offsetBy(dx: 0, dy: frame.height)
         }, completion: { _ in
            self.perform(selector, with: notificationView, afterDelay: duration)
         })
      } else {
         // Schedule calling closing selector right away
         perform(selector, with: notificationView, afterDelay: duration)
      }
   }
   
   @objc open class func closeAnnoyingNotificationOnTopOfStatusBar(_ sender: NSObject) {
      NSObject.cancelPreviousPerformRequests(withTarget: self)
      
      var notificationView: UIView?
      
      if let sender2 = sender as? UITapGestureRecognizer {
         if sender2.isKind(of: UITapGestureRecognizer.self) {
            notificationView = sender2.view!
         } else if sender2.isKind(of: UIView.self) {
            if let sender3 = sender as? UIView {
               notificationView = sender3
            }
         }
      }
      UIView.animate(withDuration: bannerDissapearAnimationDuration,
                     animations: { () -> Void in
                        if let frame = notificationView?.frame {
                           notificationView?.frame = frame.offsetBy(dx: 0, dy: -frame.size.height)
                        }
                     },
                     completion: { (_) -> Void in
                        notificationView?.removeFromSuperview()
                        
                        bannerWindow?.isHidden = true
      })
   }
   
   // MARK: - Private class methods -
   
   fileprivate class func labelForText(_ text: String) -> UILabel {
      let textSize = text.size(withAttributes: [NSAttributedString.Key.font: font])
      
      let labelRect = CGRect(x: 0,
                             y: 0,
                             width: textSize.width,
                             height: textSize.height)
      
      let label = UILabel(frame: labelRect)
      label.font = font
      label.textColor = textColor
      label.text = text as String
      label.numberOfLines = 0
      
      return label
   }
   
   fileprivate class func addMainWindowBlocker() -> UIView {
      let window = UIApplication.shared.delegate!.window!!
      
      let blocker = UIView(frame: window.bounds)
      blocker.backgroundColor = backgroundColor
      blocker.tag = containerViewTag
      
      blocker.translatesAutoresizingMaskIntoConstraints = false
      
      window.addSubview(blocker)
      
      let viewsDictionary = ["blocker": blocker]
      
      // Add constraints to handle orientation change
      let constraintsV = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[blocker]-0-|",
                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                        metrics: nil,
                                                        views: viewsDictionary)
      
      let constraintsH = NSLayoutConstraint.constraints(withVisualFormat: "|-0-[blocker]-0-|",
                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                        metrics: nil,
                                                        views: viewsDictionary)
      
      window.addConstraints(constraintsV + constraintsH)
      
      return blocker
   }
}
