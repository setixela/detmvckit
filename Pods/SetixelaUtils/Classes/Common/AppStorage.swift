//
//  AppStorage.swift
//  SetixelaUtils
//
//  Created by AlexSolo on 11.01.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

open class AppStorage<T> where T: RawRepresentable {
   public static func save(_ object: Any?, forKey: T, addString: String? = nil, suitName: String? = nil) {
      guard let keyString = forKey.rawValue as? String else {
         return
      }
      
      let fullKeyString = keyString + (addString ?? "")
      AppStorage.saveObject(object, forKey: fullKeyString, suitName: suitName)
   }
   
   public static func object(forKey: T, addString: String? = nil, suitName: String? = nil) -> Any? {
      guard let keyString = forKey.rawValue as? String else {
         return nil
      }
      
      let fullKeyString = keyString + (addString ?? "")
      return AppStorage.loadObject(forKey: fullKeyString, suitName: suitName)
   }
   
   public static func cleanUp() {
      UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
   }
   
   ///////////////
   private static func saveObject(_ object: Any?, forKey: String, suitName: String? = nil) {
      var ud: UserDefaults?
      
      if suitName == nil {
         ud = UserDefaults.standard
      } else {
         ud = UserDefaults(suiteName: suitName!)
      }
      
      ud?.set(object, forKey: forKey)
      ud?.synchronize()
   }
   
   private static func loadObject(forKey: String, suitName: String? = nil) -> Any? {
      var ud: UserDefaults?
      
      if suitName == nil {
         ud = UserDefaults.standard
      } else {
         ud = UserDefaults(suiteName: suitName!)
      }
      
      return ud?.object(forKey: forKey)
   }
   
   private static func saveBool(_ bool: Bool, forKey: String, suitName: String? = nil) {
      var ud: UserDefaults?
      
      if suitName == nil {
         ud = UserDefaults.standard
      } else {
         ud = UserDefaults(suiteName: suitName!)
      }
      
      ud?.set(bool, forKey: forKey)
      ud?.synchronize()
   }
   
   private static func loadBool(forKey: String, suitName: String? = nil) -> Bool? {
      var ud: UserDefaults?
      
      if suitName == nil {
         ud = UserDefaults.standard
      } else {
         ud = UserDefaults(suiteName: suitName!)
      }
      
      let value = ud?.bool(forKey: forKey)
      return value
   }
}
