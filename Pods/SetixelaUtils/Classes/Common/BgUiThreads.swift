//
//  BgUiThreads.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 05.07.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public func BGUtility(_ block: @escaping () -> Void) {
   DispatchQueue.global(qos: .utility).async(execute: block)
}

public func BGBackground(_ block: @escaping () -> Void) {
   DispatchQueue.global(qos: .background).async(execute: block)
}

public func BG(_ block: @escaping () -> Void) {
   DispatchQueue.global(qos: .default).async(execute: block)
}

public func UI(_ block: @escaping () -> Void) {
   DispatchQueue.main.async(execute: block)
}
