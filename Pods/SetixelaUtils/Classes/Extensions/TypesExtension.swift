//
//  TypesExtension.swift
//  Invercity
//
//  Created by AlexSolo on 09.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public extension Bool {
   init<T: BinaryInteger>(_ integer: T) {
      self.init(integer != 0)
   }
}

public extension Int {
   func format(_ format: String) -> String {
      return NSString(format: "%\(format)d" as NSString, self) as String
   }
}

public extension Double {
   func format(_ format: String) -> String {
      return NSString(format: "%\(format)f" as NSString, self) as String
   }
}

public extension CGFloat {
   static func random() -> CGFloat {
      return CGFloat(arc4random()) / CGFloat(UInt32.max)
   }
   
   func subdivide(width: CGFloat, count: Int) -> [CGFloat] {
      
      let countFloat = CGFloat(count-1)
      let step = (self-width) / countFloat
      
      var points: [CGFloat] = []
      
      for index in 0..<count {
         let point = CGFloat(index)*step
         points.append(point)
      }
      
      return points
   }
}

public extension Int {
   static func random(max: Int) -> Int {
      return Int(arc4random_uniform(UInt32(max)))
   }
}

public extension Int {
   var stringValue: String {
      return String(self)
   }
}
