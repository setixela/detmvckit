//
//  UIColorExtensions.swift
//  Linkoid
//
//  Created by Admin on 18.11.15.
//  Copyright © 2015 XYZ Games. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
	//MARK: -------------------------------------------------- hex componets hue rgb brightness
	convenience init(hex: Int, alpha: CGFloat = 1.0) {
		let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
		let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
		let blue = CGFloat((hex & 0xFF)) / 255.0
		self.init(red: red, green: green, blue: blue, alpha: alpha)
	}
	
	var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
		var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
		getRed(&r, green: &g, blue: &b, alpha: &a)
		return (r, g, b, a)
	}
	
	var componentsHue: (hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat) {
		var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
		getHue(&h, saturation: &s, brightness: &b, alpha: &a)
		return (h, s, b, a)
	}
	
	func rgb255() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)? {
		var fRed: CGFloat = 0
		var fGreen: CGFloat = 0
		var fBlue: CGFloat = 0
		var fAlpha: CGFloat = 0
		if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
			let iRed = fRed * 255
			let iGreen = fGreen * 255
			let iBlue = fBlue * 255
			let iAlpha = fAlpha * 255
			
			return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
		} else {
			return nil
		}
	}
    
	func lighterColor(_ lighter: CGFloat) -> UIColor {
		var h: CGFloat = 0
		var s: CGFloat = 0
		var b: CGFloat = 0
		var a: CGFloat = 0
		
		if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) == true {
			return UIColor(hue: h, saturation: max(s / lighter, 0.0), brightness: min(b * lighter, 1.0), alpha: a)
		}
		return self
	}
   
   func multiplyColorWith(hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat) -> UIColor {
      var _h: CGFloat = 0
      var _s: CGFloat = 0
      var _b: CGFloat = 0
      var _a: CGFloat = 0
      
      getHue(&_h, saturation: &_s, brightness: &_b, alpha: &_a)
      
      return UIColor(hue: _h * (1 - hue), saturation: _s * (1 - saturation), brightness: _b * (1 - brightness), alpha: _a * (1 - alpha))
   }
}

public extension UIColor {
   static func random() -> UIColor {
      return UIColor(red: .random(),
                     green: .random(),
                     blue: .random(),
                     alpha: 1.0)
   }
}

public extension UIColor {
   
   convenience init(hexString: String) {
      let scanner  = Scanner(string: hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
      
      if hexString.hasPrefix("#") {
         scanner.scanLocation = 1
      }
      
      var color: UInt32 = 0
      scanner.scanHexInt32(&color)
      
      let mask = 0x000000FF
      let r = Int(color >> 16) & mask
      let g = Int(color >> 8) & mask
      let b = Int(color) & mask
      
      let red   = CGFloat(r) / 255.0
      let green = CGFloat(g) / 255.0
      let blue  = CGFloat(b) / 255.0
      
      self.init(red: red, green: green, blue: blue, alpha: 1)
   }
   
   func toHexString() -> String {
      var r: CGFloat = 0
      var g: CGFloat = 0
      var b: CGFloat = 0
      var a: CGFloat = 0
      
      getRed(&r, green: &g, blue: &b, alpha: &a)
      
      let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
      
      return NSString(format: "#%06x", rgb) as String
   }
}
