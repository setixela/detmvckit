//
//  StringExtension.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 15.09.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

public extension String {
   var intValue: Int {
      return Int(self)!
   }
   
   var floatValue: Float {
      return (self as NSString).floatValue
   }
   
   var cgFloatValue: CGFloat {
      return CGFloat((self as NSString).floatValue)
   }
}

public extension String {
   subscript(i: Int) -> String {
      return self[i ..< i + 1]
   }
   
   subscript(r: Range<Int>) -> String {
      let start = index(startIndex, offsetBy: r.lowerBound)
      let end = index(startIndex, offsetBy: r.upperBound)
      return String(self[start ..< end])
   }
}

// MARK: -------------------------------------- validations

public extension String {
   // To check text field or String is blank or not
   var isBlank: Bool {
      let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
      return trimmed.isEmpty
   }
   
   // Validate Email
   
   var isEmail: Bool {
      do {
         let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
         return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.count)) != nil
      } catch {
         return false
      }
   }
   
   var isAlphanumeric: Bool {
      return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
   }
   
   // validate Password
   var isValidPassword: Bool {
      guard self.count >= 6 else {
         return false
      }
      
      do {
         let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
         if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.count)) != nil {
            if self.count >= 6 && self.count <= 20 {
               return true
            } else {
               return false
            }
         } else {
            return false
         }
      } catch {
         return false
      }
   }
   
   var isValidPhoneNumber: Bool {
      let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
      let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
      let result = phoneTest.evaluate(with: self)
      return result
   }
   
   var isLetters: Bool {
      let charSet = CharacterSet.letters
      if self.rangeOfCharacter(from: charSet) != nil {
         return true
      } else {
         return false
      }
   }
   
   var isPunctuations: Bool {
      let charSet = CharacterSet.punctuationCharacters
      if self.rangeOfCharacter(from: charSet) != nil {
         return true
      } else {
         return false
      }
   }
}

public extension String {
   func removeSpecialCharsFromString() -> String {
      let okayChars: Set<Character> = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890абвгдеёжзиклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ")
      return String(self.filter { okayChars.contains($0) })
   }
   
   func specialCharactersRemoved() -> String {
      let okayChars: Set<Character> =
         Set(" 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890абвгдеёжзиклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ")
      return String(self.filter { okayChars.contains($0) })
   }
   
   func removedLeadAndTrailSpaces() -> String {
      return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}

public extension String {
   func getHeightFor(fontSize: CGFloat, width: CGFloat) -> CGFloat {
      let font = UIFont.systemFont(ofSize: fontSize)
      let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineBreakMode = .byWordWrapping
      let attributes = [NSAttributedString.Key.font: font,
                        NSAttributedString.Key.paragraphStyle: paragraphStyle.copy()]
      
      let text = self as NSString
      let rect = text.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
      return rect.size.height
   }
}

public extension String {
   static func random(maxLength: Int) -> String {
      let minL: Int = 3
      let length = minL + Int.random(max: (maxLength - minL))
      let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" // 0123456789"
      let len = UInt32(letters.length)
      
      var randomString = ""
      
      for i in 0 ..< length {
         let rand = arc4random_uniform(len)
         var nextChar = letters.character(at: Int(rand))
         if i % (4 + Int.random(max: 6)) == 0 && (i != 0) {
            randomString += " "
            randomString += (NSString(characters: &nextChar, length: 1) as String).capitalized
         } else {
            if i == 0 {
               randomString += (NSString(characters: &nextChar, length: 1) as String).capitalized
            } else {
               randomString += (NSString(characters: &nextChar, length: 1) as String).lowercased()
            }
         }
      }
      
      return randomString
   }
}

public extension String {
   static func documentsPathForFileName(name: String) -> String {
      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
      let path = paths[0] as NSString
      let fullPath = path.appendingPathComponent(name)
      
      return fullPath
   }
}

public extension String {
   static func attributedString(from string: String, nonBoldRange: NSRange?) -> NSMutableAttributedString {
      let fontSize = UIFont.systemFontSize
      let attrs = [
         NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: fontSize),
         NSAttributedString.Key.foregroundColor: UIColor.black
      ]
      let nonBoldAttribute = [
         NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)
      ]
      let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
      if let range = nonBoldRange {
         attrStr.setAttributes(nonBoldAttribute, range: range)
      }
      return attrStr
   }
}

public extension String {
   func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + dropFirst()
   }
   
   mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
   }
}

public extension String {
   func removeNewLinesAndWhitespacesAtBegining() -> String {
      let trimmed = (self as NSString).trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
      return trimmed
   }
   
   func removeDoubleNewLines() -> String {
      var text = self
      
      while let rangeToReplace = text.range(of: "\n\n") {
         text.replaceSubrange(rangeToReplace, with: "\n")
      }
      
      return text
   }
   
   func removeTrippleNewLines() -> String {
      var text = self
      
      while let rangeToReplace = text.range(of: "\n\n\n") {
         text.replaceSubrange(rangeToReplace, with: "\n\n")
      }
      
      return text
   }
   
   func removeDoubleNewLinesWithTab() -> String {
      var text = self
      
      while let rangeToReplace = text.range(of: "\n\n") {
         text.replaceSubrange(rangeToReplace, with: "\n\t")
      }
      
      return text
   }
   
   func removeTrippleNewLinesWithTab() -> String {
      var text = self
      
      while let rangeToReplace = text.range(of: "\n\n\n") {
         text.replaceSubrange(rangeToReplace, with: "\n\n\t")
      }
      
      return text
   }
   
   func addTabAfterTwoNewLines() -> String {
      return self.replacingOccurrences(of: "\n\n", with: "\n\n\t")
   }
   
   func removeNewLineBetweenSymbols() -> String {
      var finalText = self
      let text = self
      var shift = 0
      
      for (idx, char) in text.enumerated() where char == "\n" {
         if idx > 0 && idx < text.count {
            let prevChar = text[idx - 1]
            let nextChar = text[idx + 1]
            
            if prevChar.isLetters && nextChar.isLetters {
               finalText.removeSubrange(Range(NSRange(location: idx - shift, length: 1), in: finalText)!)
               shift += 1
            }
         }
      }
      
      return finalText
   }
}
