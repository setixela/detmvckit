//
//  UIImageExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 06.06.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension UIImage {
   class func imageWithImage(_ image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
      UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
      image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
      
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      return newImage!
   }
   
   func scaledImage(toMaxSize maxSize: CGFloat) -> UIImage {
      let size = self.size
      let newSize = size.rescaled(toMaxSize: maxSize)
      let scaledImage = UIImage.imageWithImage(self, scaledToSize: newSize)
      
      return scaledImage
   }
   
   func tint(with color: UIColor) -> UIImage {
      UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
      guard let context = UIGraphicsGetCurrentContext() else { return self }
      
      // flip the image
      context.scaleBy(x: 1.0, y: -1.0)
      context.translateBy(x: 0.0, y: -self.size.height)
      
      // multiply blend mode
      context.setBlendMode(.multiply)
      
      let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
      context.clip(to: rect, mask: self.cgImage!)
      color.setFill()
      context.fill(rect)
      
      // create UIImage
      guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
      UIGraphicsEndImageContext()
      
      return newImage
   }
}
