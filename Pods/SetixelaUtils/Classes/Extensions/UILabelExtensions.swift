//
//  UILabelExtensions.swift
//  SetixelaUtils
//
//  Created by AlexSolo on 15.01.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import UIKit

@objc extension UILabel {
   public class func getStringHeight(_ mytext: String, fontSize: CGFloat, width: CGFloat) -> CGFloat {
      let font = UIFont.systemFont(ofSize: fontSize)
      let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineBreakMode = .byWordWrapping
      let attributes = [NSAttributedString.Key.font: font,
                        NSAttributedString.Key.paragraphStyle: paragraphStyle.copy()]
      
      let text = mytext as NSString
      let rect = text.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
      return rect.size.height
   }
   
   public class func getNumberOfLines(_ yourString: String, labelWidth: CGFloat, labelHeight: CGFloat, font: UIFont) -> Int {
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.minimumLineHeight = labelHeight
      paragraphStyle.maximumLineHeight = labelHeight
      paragraphStyle.lineBreakMode = .byWordWrapping
      
      let attributes: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle]
      
      let constrain = CGSize(width: labelWidth, height: CGFloat(Float.infinity))
      
      let size = yourString.size(withAttributes: attributes)
      let stringWidth = size.width
      
      let numberOfLines = ceil(Double(stringWidth / constrain.width))
      
      return Int(numberOfLines)
   }
   
   open func setLabelAutoHeight() {
      let size = CGSize(width: self.frame.width, height: CGFloat.greatestFiniteMagnitude)
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineBreakMode = .byWordWrapping
      let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle.copy()]
      let text = self.text! as NSString
      let rect = text.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
      self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: rect.size.height)
   }
}

public extension UILabel {
   public convenience init(toView: UIView, text: String?, textColor: UIColor?, font: UIFont?, aligment: NSTextAlignment) {
      self.init()
      toView.addSubview(self)
      
      self.textAlignment = aligment
      self.font = font
      self.textColor = textColor
      self.text = text
   }
}
