//
//  UIImageViewExtensions.swift
//  SetixelaUtils
//
//  Created by AlexSolo on 15.01.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

/////////////////////////////////////////////////////////////////
//MARK: -------------------------------------- UIImageView extend
public extension UIImageView {
   func setImageAnimated(image: UIImage?, time: TimeInterval, isInstant: Bool = false, fromZero: Bool = true, alphaMax: CGFloat = 1) {
      guard !isInstant else {
         self.alpha = alphaMax
         self.image = image
         return
      }
      
      if fromZero {
         self.alpha = 0
      } else {
         self.alpha = self.alpha<alphaMax ? self.alpha : 0
      }
      
      self.image = image
      UIView.animate(withDuration: time) {
         self.alpha = alphaMax
      }
   }
}

public extension UIImageView {
   
   func setURL(_ URL: String) {
      guard let url = Foundation.URL(string: URL) else { return }
      
      URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
         guard let httpURLResponse = response as? HTTPURLResponse, error == nil && httpURLResponse.statusCode == 200,
            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
            let data = data,
            let image = UIImage(data: data)
            else { return }
         
         DispatchQueue.main.async {
            self.image = image
         }
      }) .resume()
   }
}
