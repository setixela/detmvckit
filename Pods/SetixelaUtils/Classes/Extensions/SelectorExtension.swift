//
//  SelectorExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 11.03.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

extension Selector {
   static var didTapButton: Selector {
      return Selector(("didTapButton"))
   }
}
