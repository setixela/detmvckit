//
//  BoolExtensions.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 16.08.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension Bool {
   public func cgFloatValue() -> CGFloat {
      if self == true {
         return 1.0
      } else {
         return 0.0
      }
   }

   public func intValue() -> Int {
      if self == true {
         return 1
      } else {
         return 0
      }
   }
}
