//
//  EmbeddableController.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 26.02.2018.
//  Copyright © 2018 Gaozhiyuan. All rights reserved.
//

import Foundation

public protocol EmbeddableController {
   func embed(toViewController viewController: UIViewController, frame: CGRect)
   func embed(toNavigationController navigationController: UINavigationController, frame: CGRect)
}

extension EmbeddableController where Self: UIViewController {
   public func embed(toViewController viewController: UIViewController, frame: CGRect) {
      self.view.frame = frame
      viewController.view.addSubview(self.view)
   }
   public func embed(toNavigationController navigationController: UINavigationController, frame: CGRect) {
      self.view.frame = frame
      navigationController.view.addSubview(self.view)
      
   }
}
