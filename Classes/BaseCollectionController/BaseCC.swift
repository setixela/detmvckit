//
//  BaseCC.swift
//  GdeDuet2
//
//  Created by setiXela on 29/04/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation
import SetixelaUtils

open class BaseCC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   var collectionView: UICollectionView!
   var kSectionInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
   var layout = UICollectionViewFlowLayout()
   
   override open func viewDidLoad() {
      super.viewDidLoad()
      
      collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: UICollectionViewFlowLayout())
      collectionView.delegate = self
      collectionView.dataSource = self
      collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
      collectionView.isScrollEnabled = true
      collectionView.showsHorizontalScrollIndicator = true
      collectionView.showsVerticalScrollIndicator = false
      
      layout.scrollDirection = .horizontal
      collectionView.backgroundColor = UIColor.clear
      
      collectionView.collectionViewLayout = layout
      view.addSubview(collectionView)
   }
   
   open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 0
   }
   
   open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      return UICollectionViewCell()
   }
}
