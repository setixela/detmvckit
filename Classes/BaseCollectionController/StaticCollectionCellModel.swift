//
//  CollectionCellModel.swift
//  Invercity
//
//  Created by AlexSolo on 02.05.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public class StaticCollectionCellModel: CollectionCellTypable, SubModellable, Contentable {
   public var subModel: Contentable?
   public var cellType: EnumCollectionCells!
   
   var actionClosure: (() -> Void)?
   
   public required init(model: Contentable?, cellType: EnumCollectionCells) {
      self.subModel = model
      self.cellType = cellType
   }
   
   public required init(cellType: EnumCollectionCells) {
      self.subModel = nil
      self.cellType = cellType
   }
}
