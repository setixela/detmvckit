//
//  BaseCCD.swift
//  GdeDuet2
//
//  Created by setiXela on 29/04/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation
import SetixelaUtils

open class BaseCCD<CT: EnumCollectionCells>: BaseCC, DataControllableCC, ActionDelegate, UICollectionViewDelegateFlowLayout {
   open func perform(_ action: EnumActions) {
      print()
   }
   
   public var cellPrototype: BaseCollectionCellProtocol = BaseCollectionCell()
   
   open func reloadDataAnimated(_ animated: Bool) {
      print()
   }
   
   public var cellReuseIdentifier: String = "Cell"
   private var registeredCellTypes: [CT] = []
   // public var cellPrototype: CellBindable = BaseCollectionCell()
   
   public var models: [Contentable] = []
   public var dataView: DataViewable {
      return collectionView
   }
   
   open func reloadData() {
      collectionView.reloadData()
   }
   
   func reloadDataAnimated() {}
   open func updateCell(indexPath: IndexPath, model: Contentable?) {}
   
   func numberOfSections(in collectionView: UICollectionView) -> Int {
      return 1
   }
   
   open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return models.count
   }
   
   open override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let model = models[indexPath.row]
      if let vc = self as? Paginable {
         vc.checkAndLoadNextPage(models: models, indexPath: indexPath)
      }
      
      return cellFor(model: model, indexPath: indexPath)
   }
   
   func cellFor(model: Contentable, indexPath: IndexPath) -> UICollectionViewCell {
      var cell: BaseCollectionCellProtocol
      // var cellType: CT?
      if let type = (model as? CellTypable)?.cellType as? CT {
         // cellType = type
         cell = (dequeueReusableCell(cellType: type, forIndexPath: indexPath) as? BaseCollectionCellProtocol)!
      } else {
         // cellType = registeredCellTypes.first
         cell = (collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as? BaseCollectionCellProtocol)!
      }
      
      cell.actionDelegate = self
      
      if model is StaticCellModel {
         if let subModel = (model as? StaticCellModel)?.subModel {
            return ((cell.bind(subModel) as? UICollectionViewCell)!)
         }
      }
      
      cell.indexPath = indexPath
      
      return (cell.bind(model) as? UICollectionViewCell) ?? UICollectionViewCell()
   }
   
   open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      guard models.count > 0 else {
         return CGSize(width: 50, height: 50)
      }
      
      let model = models[indexPath.row]
      return sizeFor(model: model, indexPath: indexPath)
   }
   
   func sizeFor(model: Contentable, indexPath: IndexPath) -> CGSize {
      var size: CGSize
      
      if let type = (model as? CollectionCellTypable)?.cellType {
         if let prototype = type.prototype as? CollectionCellDynamicSizeable {
            size = prototype.dynamicSize(model: model)
         } else {
            size = type.prototype.cellSize
         }
      } else {
         let prototype = cellPrototype
         let classType = type(of: prototype)
         size = classType.kSize()
      }
      
      if let model = model as? StaticCollectionCellModel,
         let subModel = model.subModel,
         let cellType = model.cellType {
         if let prototype = cellType.prototype as? CollectionCellDynamicSizeable {
            size = prototype.dynamicSize(model: subModel)
         } else {
            size = cellType.prototype.cellSize
         }
      }
      
      return size
   }
   
   open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return kSectionInsets
   }
   
   open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 1
   }
}

extension BaseCCD {
   public func performAction(object: ActionObject) {
      print("Override!!!! func performAction(object: ActionObject) ")
   }
}

extension BaseCCD {
}

extension BaseCCD {
   public func registerClass(cellType: EnumCollectionCells) {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      collectionView.register(type(of: (prototype as AnyObject)), forCellWithReuseIdentifier: className)
      cellPrototype = prototype
   }
   
   public func registerNib(cellType: EnumCollectionCells) {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      collectionView.register(UINib(nibName: className, bundle: nil), forCellWithReuseIdentifier: className)
      cellPrototype = prototype
   }
   
   public func registerClasses(cellTypes: [EnumCollectionCells]) {
      for type in cellTypes {
         registerClass(cellType: type)
      }
   }
   
   func dequeueReusableCell(cellType: EnumCollectionCells, forIndexPath: IndexPath) -> UICollectionViewCell {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: className, for: forIndexPath)
      
      return cell
   }
}
