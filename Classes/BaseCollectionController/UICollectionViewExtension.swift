//
//  UICollectionViewExtension.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 18.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import UIKit

/////////////////////////////////////////////////////////////////

// MARK: -------------------------------- UICollectionView extends

extension UICollectionView: DataViewable {}

public extension UICollectionView {
   public static var defaultCellIdentifier: String {
      return "Cell"
   }

   public func registerClass(_ anyClass: AnyClass, cellIdentifier: String, bundleIdentifier: String?) {
      self.register(anyClass, forCellWithReuseIdentifier: cellIdentifier)
   }

   public func registerNib(_ nibName: String, cellIdentifier: String = defaultCellIdentifier, bundleIdentifier: String? = nil) {
      self.register(UINib(nibName: nibName,
                          bundle: bundleIdentifier != nil ? Bundle(identifier: bundleIdentifier!) : nil),
                    forCellWithReuseIdentifier: cellIdentifier)
   }

   public subscript(indexPath: IndexPath) -> UICollectionViewCell {
      return self.dequeueReusableCell(withReuseIdentifier: UICollectionView.defaultCellIdentifier, for: indexPath)
   }

   public subscript(indexPath: IndexPath, identifier: String) -> UICollectionViewCell {
      return self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
   }
}
