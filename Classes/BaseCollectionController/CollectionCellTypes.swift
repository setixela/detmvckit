//
//  CollectionCellTypes.swift
//  Invercity
//
//  Created by AlexSolo on 02.05.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

public protocol EnumCollectionCells {
   var prototype: BaseCollectionCellProtocol { get }
}
