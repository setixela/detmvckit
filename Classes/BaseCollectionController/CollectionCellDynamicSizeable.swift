//
//  CollectionCellDynamicSizeable.swift
//  GdeDuet2
//
//  Created by setiXela on 29/04/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation
import SetixelaUtils

protocol CollectionCellDynamicSizeable: class {
   func dynamicSize(model: Contentable) -> CGSize
}
