//
//  InsetLabel.swift
//  Invercity
//
//  Created by AlexSolo on 21.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

public class InsetLabel: UILabel {
   
   override public init(frame: CGRect) {
      super.init(frame: frame)
      self.configure()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.configure()
   }
   
   public var insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
   // Only override draw() if you perform custom drawing.
   // An empty implementation adversely affects performance during animation.
   
   override public func drawText(in rect: CGRect) {
      super.drawText(in: rect.inset(by: insets))
   }
   
   public func setLeftInset(leftInset: CGFloat) {
      insets.left = leftInset
   }
   
   public func setRightInset(rightInset: CGFloat) {
      insets.right = rightInset
   }
   
   open override func setLabelAutoHeight() {
      let height = self.text!.getHeightFor(fontSize: 14, width: self.width-insets.left-insets.right) + insets.top + insets.bottom
      self.height = height
   }

   public func setLabelAutoHeight(maxHeight: CGFloat) {
      if self.text != nil {
         var height = self.text!.getHeightFor(fontSize: 14, width: self.width-insets.left-insets.right) + insets.top + insets.bottom
         if height > maxHeight {
            height = maxHeight
         }
         self.height = height
      } else {
         self.height = 0
      }
   }

   public func configure() {}
}
