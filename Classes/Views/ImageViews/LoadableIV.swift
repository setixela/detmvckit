//
//  LoadableIV.swift
//  GdeDuet2
//
//  Created by setiXela on 23/04/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

open class LoadableIV: UIImageView, Loadable, Configurable {
   public var activityIndicator: UIActivityIndicatorView?
   
   override public init(frame: CGRect) {
      super.init(frame: frame)
      
      self.configure()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
   
   convenience public init() {
      self.init(frame: CGRect.zero)
      
   }
   
   open func configure() {
      print("Override loadableIV configure")
   }
}
