//
//  LeftRightTextLabelProtocols.swift
//  Invercity
//
//  Created by AlexSolo on 17.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

/////////////////////
public protocol LeftTextCellProtocol: class {
   var leftTextLabel: UILabel! {get set}
   func configureLeftText(leftOffset: CGFloat)
   func setLeftText(text: String)
}

extension LeftTextCellProtocol where Self: UITableViewCell {
   public  func configureLeftText(leftOffset: CGFloat) {
      leftTextLabel = UILabel(frame: contentView.bounds)
      leftTextLabel.width = contentView.width - leftOffset*2.0
      leftTextLabel.left = leftOffset
      leftTextLabel.font = UIFont.systemFont(ofSize: 16)//, weight: UIFont.Weight.light)
      leftTextLabel.textColor = UIColor.black
      leftTextLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      leftTextLabel.textAlignment = .left
      
      leftTextLabel.backgroundColor = UIColor.clear
      
      self.addSubview(leftTextLabel)
   }
   public func setLeftText(text: String) {
      leftTextLabel.text = text
      leftTextLabel.sizeToFit()
      leftTextLabel.center.y = self.height/2.0
   }
}
/////////////////////

public protocol RightTextCellProtocol: class {
   var rightTextLabel: UILabel! {get set}
   func configureRightText(rightOffset: CGFloat)
   func setRightText(text: String)
}

extension RightTextCellProtocol where Self: UITableViewCell {
   public func configureRightText(rightOffset: CGFloat) {
      rightTextLabel = UILabel(frame: contentView.bounds)
      rightTextLabel.width = contentView.width - rightOffset
      rightTextLabel.font = UIFont.systemFont(ofSize: 16)//, weight: UIFont.Weight.light)
      rightTextLabel.textColor = UIColor.lightGray
      rightTextLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      rightTextLabel.textAlignment = .right
      
      self.addSubview(rightTextLabel)
   }
   public func setRightText(text: String) {
      rightTextLabel.text = text
   }
}
/////////////////////
public protocol LeftImageAndTextCellProtocol: class {
   var leftTextLabel: UILabel! {get set}
   var leftImageView: UIImageView! {get set}
   
   func configureLeftImageAndText(leftOffset: CGFloat)
   func setLeftText(text: String)
   func setLeftImage(image: UIImage)
}

extension LeftImageAndTextCellProtocol where Self: UITableViewCell {
   public func configureLeftImageAndText(leftOffset: CGFloat) {
      
      let imageSize = contentView.height - leftOffset
      leftImageView = UIImageView(frame: CGRect(x: leftOffset,
                                                y: contentView.height/2.0 - imageSize/2.0,
                                                width: imageSize,
                                                height: imageSize))
      leftImageView.contentMode = .scaleAspectFit
      leftImageView.center.y = self.contentView.frame.size.height / 2.0
      self.addSubview(leftImageView)
      
      leftTextLabel = UILabel(frame: contentView.bounds)
      leftTextLabel.width = (contentView.width - leftOffset*2.0)-leftImageView.right - leftOffset
      leftTextLabel.left = leftOffset/2 + leftImageView.right
      leftTextLabel.font = UIFont.systemFont(ofSize: 16)//, weight: UIFont.Weight.light)
      leftTextLabel.textColor = UIColor.black
      leftTextLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      leftTextLabel.textAlignment = .left
      
      leftTextLabel.backgroundColor = UIColor.clear
      
      self.addSubview(leftTextLabel)
   }
   public func setLeftText(text: String) {
      leftTextLabel.text = text
      // leftTextLabel.sizeToFit()
      leftTextLabel.center.y = self.height/2.0
   }
   public func setLeftImage(image: UIImage) {
      leftImageView.image = image
   }
}
/////////////////////
public protocol RightSwitchCellProtocol: class {
   
   var rightSwitch: UISwitch! {get set}
   func configureRightSwitch(rightOffset: CGFloat)
}

extension RightSwitchCellProtocol where Self: UITableViewCell {
   public func configureRightSwitch(rightOffset: CGFloat) {
      
      rightSwitch = UISwitch()
      rightSwitch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
      rightSwitch.right = contentView.width * screenCoef - rightOffset
      rightSwitch.center.y = contentView.height/2.0 + (rightSwitch.height-rightSwitch.height*0.7) / (2*0.7)
      rightSwitch.backgroundColor = UIColor.clear
      self.contentView.addSubview(rightSwitch)
      
   }
}
