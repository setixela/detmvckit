//
//  LeftTextCells.swift
//  Invercity
//
//  Created by AlexSolo on 22.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

fileprivate let kLeftOffset: CGFloat = 16

open class LeftTextCell: BaseCell, LeftTextCellProtocol {
   public var leftTextLabel: UILabel!
   
   override open func configure() {
      configureLeftText(leftOffset: kLeftOffset)
   }
}

open class DetailTextCell: BaseCell {
   override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
}
