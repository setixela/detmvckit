//
//  TextFieldsProtocol.swift
//  Invercity
//
//  Created by AlexSolo on 21.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public protocol LeftTextFieldProtocol: class {
   var leftTextField: TextField! {get set}
   func configureLeftTextField()
}

extension LeftTextFieldProtocol where Self: BaseCell {
   public func configureLeftTextField() {
      leftTextField = TextField(frame: contentView.bounds)
      leftTextField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
      leftTextField.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
      leftTextField.textColor = UIColor.lightGray
      leftTextField.textAlignment = .left
      
      self.addSubview(leftTextField)
   }
}

///////////////////////////////////
public protocol RightTextFieldProtocol: class {
   var rightTextField: TextField! {get set}
   func configureRightTextField()
}

extension RightTextFieldProtocol where Self: BaseCell {
   public func configureRightTextField() {
      rightTextField = TextField(frame: contentView.bounds)
      rightTextField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
      rightTextField.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
      rightTextField.textColor = UIColor.lightGray
      rightTextField.textAlignment = .right
      
      self.addSubview(rightTextField)
   }
}
