//
//  DisclosureCells.swift
//  Invercity
//
//  Created by AlexSolo on 16.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation
import SetixelaUtils

fileprivate let kLeftOffset: CGFloat = 16
//////////////////
public protocol DisclosureButtonTappable: class {
   var disclosureButton: UIButton! {get set}
   func didTapDisclosureButton(sender: UIButton)
   
   func setupDisclosure(button: UIButton)
}

open class LeftTextDisclosureCell: LeftTextCell, DisclosureButtonTappable {
   public var disclosureButton: UIButton!

   public func setupDisclosure(button: UIButton) {
      
      disclosureButton = button
      disclosureButton.addTarget(self, action: #selector(didTapDisclosureButton(sender:)), for: .touchUpInside)
      
      self.accessoryView = disclosureButton
      
   }
   
   override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: .default, reuseIdentifier: reuseIdentifier)
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   @objc open func didTapDisclosureButton(sender: UIButton) {
      //override this
      print("Need override: func didTapDisclosureButton(sender: UIButton)")
   }

   override open func layoutSubviews() {
      
      super.layoutSubviews()
      
      let maxWidth = cV.width - (leftTextLabel!.left + kGrid8)// + disclosureButton.width)
    //  if leftTextLabel!.width > maxWidth {
         leftTextLabel?.width = maxWidth
     // }
   }

}

open class DetailTextDisclosureCell: BaseCell, DisclosureButtonTappable {
   
   public var disclosureButton: UIButton!//(type: UIButtonType.custom)
   
   override open func configure() {
      super.configure()
      
   }
   
   public func setupDisclosure(button: UIButton) {
      
      disclosureButton = button
      disclosureButton.addTarget(self, action: #selector(didTapDisclosureButton(sender:)), for: .touchUpInside)
      
      self.accessoryView = disclosureButton
      
   }
   //
   override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   @objc open func didTapDisclosureButton(sender: UIButton) {
      //override this
      print("Need override: func didTapDisclosureButton(sender: UIButton)")
   }
}

open class TextImageDisclosureViewCell: BaseCell {
   public var iconIV: PlaceholdedImageView = PlaceholdedImageView()
   
   override open func configure() {
      super.configure()
      
      cV.addSubview(iconIV)
   }

   override open func layoutSubviews() {
      
      super.layoutSubviews()
      
      iconIV.frame.size = CGSize(width: kGrid8*4+4, height: kGrid8*4+4)
      iconIV.left = kLeftOffset
      iconIV.center.y = cV.height/2
      
      //- kGrid8/4
      textLabel?.left = iconIV.right + kLeftOffset
      textLabel?.top = iconIV.top

      textLabel?.height = iconIV.height
      
      let maxWidth = cV.width - (textLabel!.left + kGrid8*2)
      if textLabel!.width>maxWidth {
         textLabel?.width = maxWidth
      }
   }

}

open class DetailImageDisclosureCell: DetailTextDisclosureCell {
   
   public var iconIV: PlaceholdedImageView = PlaceholdedImageView()
   public var isDetailTextHidden: Bool = false {
      didSet {
         self.detailTextLabel?.isHidden = self.isDetailTextHidden
      }
   }
   
   override open func configure() {
      super.configure()
      
      cV.addSubview(iconIV)
   }
   
   override open func layoutSubviews() {
      
      super.layoutSubviews()
      
      iconIV.frame.size = CGSize(width: kGrid8*4+4, height: kGrid8*4+4)
      iconIV.left = kLeftOffset
      iconIV.center.y = cV.height/2
      
      //- kGrid8/4
      textLabel?.left = iconIV.right + kLeftOffset
      textLabel?.top = iconIV.top
      if isDetailTextHidden {
         textLabel?.height = iconIV.height
      } else {
         textLabel?.height = iconIV.height/2 + 2
         
         detailTextLabel?.height = iconIV.height/2 - kGrid8/4
         detailTextLabel?.left = iconIV.right + kLeftOffset
         detailTextLabel?.bottom = iconIV.bottom
      }
      
      let maxWidth = cV.width - (textLabel!.left + disclosureButton.width - kGrid8*2)
      if textLabel!.width>maxWidth {
         textLabel?.width = maxWidth
      }
      if detailTextLabel!.width>maxWidth {
         detailTextLabel?.width = maxWidth
      }
   }
}
