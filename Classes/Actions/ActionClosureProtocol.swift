//
//  ActionClosureProtocol.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 30.07.2018.
//  Copyright © 2018 Gaozhiyuan. All rights reserved.
//

import Foundation

public typealias DefaultCompletion = (_ success: Bool) -> Void
public typealias DefaultClosure = () -> Void
public typealias DefaultSelfClosure = (Contentable) -> Void

public protocol ActionClosureProtocol: Contentable {
   var actionClosure: DefaultClosure? { get set }
}

public protocol ActionSelfClosureProtocol: Contentable {
   var actionSelfClosure: DefaultSelfClosure? { get set }
}

public extension Array where Element: ActionSelfClosureProtocol {
   func addDidSelectSelfClosure(_ closure: @escaping DefaultSelfClosure) -> [ActionSelfClosureProtocol] {
      return map {
         var item = $0
         item.actionSelfClosure = closure
         return item
      }
   }
}
