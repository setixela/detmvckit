import Foundation

public protocol EnumActions {
}

public protocol ActionObjectProtocol {
   var type: EnumActions { get set }
   var value: Any? { get set }
   var indexPath: IndexPath? { get set }
   var completion: ((_ success: Bool) -> Void)? { get set }

   init(type: EnumActions, value: Any?, indexPath: IndexPath?, completion: ((_ success: Bool) -> Void)?)
}

//////
public class ActionObject {
   public var type: EnumActions
   public var value: Any?
   public var indexPath: IndexPath?
   public var completion: ((_ success: Bool) -> Void)?

   public required init(type: EnumActions, value: Any? = nil, indexPath: IndexPath? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
      self.type = type
      self.value = value
      self.indexPath = indexPath
      self.completion = completion
   }

   public func intValue() -> Int? {
      return value as? Int
   }

   public func cgfloatValue() -> CGFloat? {
      return value as? CGFloat
   }

   public func dateValue() -> Date? {
      return value as? Date
   }

   public func stringValue() -> String? {
      return value as? String
   }

   public func contentValue() -> Contentable? {
      return value as? Contentable
   }
}

///////
public protocol ActionSender: class {
   var actionDelegate: ActionDelegate? { get set }
}

public protocol ActionDelegate: class {
   func perform(_ action: EnumActions)

   @available(*, deprecated)
   func performAction(object: ActionObject)

   @available(*, deprecated)
   func performAction(type: EnumActions, model: Contentable?, indexPath: IndexPath?, completion: ((Bool) -> Void)?)
}

extension ActionDelegate {
   public func performAction(type: EnumActions, model: Contentable? = nil, indexPath: IndexPath? = nil, completion: ((Bool) -> Void)? = nil) {
      let ao = ActionObject(type: type, value: model, indexPath: indexPath, completion: completion)
      self.performAction(object: ao)
   }
}
