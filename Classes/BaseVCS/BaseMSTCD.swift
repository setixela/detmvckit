//
//  MultiSectionalTC.swift
//  Invercity
//
//  Created by AlexSolo on 17.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public protocol MultiSectionsProtocol {
   var modelsArray: [[Contentable]] { get set }
}

open class BaseMSTCD<C: EnumCells>: BaseTCD<C>, MultiSectionsProtocol {
   public var modelsArray: [[Contentable]] = [[]]
   
   open override func cleanAllData() {
      super.cleanAllData()
      modelsArray = [[]]
   }
   
   open override func numberOfSections(in tableView: UITableView) -> Int {
      return modelsArray.count
   }
   
   open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let models = modelsArray[section]
      let count = models.count
      return count
   }
   
   open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let models = modelsArray[indexPath.section]
      if models.count - 1 >= indexPath.row {
         let model = models[indexPath.row]
         
         return heightFor(model: model, indexPath: indexPath)
      } else {
         return 0
      }
   }
   
   open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let section = indexPath.section
      let row = indexPath.row
      
      let models = modelsArray[section]
      let model = models[row]
      
      return cellFor(model: model, indexPath: indexPath)
   }
   
   open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.tableView.deselectRow(at: indexPath, animated: true)
      
      let models = modelsArray[indexPath.section]
      
      let model = models[indexPath.row]
      if let model = model as? ActionClosureProtocol {
         model.actionClosure?()
      } else {
         if let model = model as? ActionSelfClosureProtocol {
            model.actionSelfClosure?(model)
         } else {
            if actionSelfClosure != nil {
               actionSelfClosure?(model)
            } else {
               if let cell = tableView.cellForRow(at: indexPath) as? BaseCellProtocol {
                  cell.actionClosure?()
               }
            }
         }
      }
   }
   
   //// ContentMarkable
   public func markedModelsCount(section: Int) -> Int {
      var count = 0
      for content in modelsArray[section] where content is ContentMarkable {
         if var model = content as? ContentMarkable {
            if model.isMarked {
               count += 1
            }
         }
      }
      return count
   }
   
   public func markAllModels(isMarked: Bool, section: Int) {
      for content in modelsArray[section] where content is ContentMarkable {
         if var model = content as? ContentMarkable {
            model.isMarked = isMarked
         }
      }
   }
   
   public func markedModels(isMarked: Bool, section: Int) -> [Contentable] {
      return modelsArray[section].filter({ (content) -> Bool in
         if content is ContentMarkable {
            if var model = content as? ContentMarkable {
               if isMarked {
                  return model.isMarked
               } else {
                  return !model.isMarked
               }
            } else {
               return false
            }
         } else {
            return false
         }
      }) as [Contentable]
   }
   
   public override func markedIndexPaths(isMarked: Bool, section: Int) -> [IndexPath] {
      var indexPaths: [IndexPath] = []
      for (idx, content) in modelsArray[section].enumerated() where content is ContentMarkable {
         if let model = content as? ContentMarkable {
            if isMarked {
               if model.isMarked {
                  let indexPath = IndexPath(row: idx, section: section)
                  indexPaths.append(indexPath)
               }
            } else {
               if !model.isMarked {
                  let indexPath = IndexPath(row: idx, section: section)
                  indexPaths.append(indexPath)
               }
            }
         }
      }
      return indexPaths
   }
}
