//
//  BaseTableView.swift
//  GdeDuet2
//
//  Created by AlexSolo on 10.05.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public class BaseTableView: UITableView, Loadable {
   
   public var activityIndicator: UIActivityIndicatorView?
   
}
