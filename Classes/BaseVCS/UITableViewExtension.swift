//
//  UITableViewExtension.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 18.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import UIKit

/////////////////////////////////////////////////////////////////
//MARK: -------------------------------------- UITableView extension

extension UITableView: DataViewable {}
//extension UICollectionView: DataViewable {}

public extension UITableView {
   
   public static var defaultCellIdentifier: String {
      return "Cell"
   }
   
   public func registerClass(_ anyClass: AnyClass, cellIdentifier: String, bundleIdentifier: String?) {
      self.register(anyClass, forCellReuseIdentifier: cellIdentifier)
   }
   
   public func registerNib(_ nibName: String, cellIdentifier: String = defaultCellIdentifier, bundleIdentifier: String? = nil) {
      self.register(UINib(nibName: nibName,
                          bundle: bundleIdentifier != nil ? Bundle(identifier: bundleIdentifier!) : nil),
                    forCellReuseIdentifier: cellIdentifier)
   }
   
   public subscript(indexPath: IndexPath) -> UITableViewCell {
      return self.dequeueReusableCell(withIdentifier: UITableView.defaultCellIdentifier, for: indexPath)
   }
   
   public subscript(indexPath: IndexPath, identifier: String) -> UITableViewCell {
      return self.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
   }
   
}

public extension UITableView {
   public func update() {
      self.beginUpdates()
      self.endUpdates()
   }
}
