//
//  BaseTC
//  GdeDuet2
//
//  Created by Admin on 13.05.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation
import SetixelaUtils
import UIKit

open class BaseTC: BaseVC, UITableViewDelegate, UITableViewDataSource {
   public var models: [Contentable] = []
   
   public var tableView: BaseTableView!
   public var headerTitles: [String?] = []
   public var isTouch: Bool!
   
   public var headerParameters: (backColor: UIColor, fontColor: UIColor, fontSize: CGFloat, height: CGFloat) = (backColor: UIColor.lightGray, fontColor: UIColor.darkGray, fontSize: 12, height: 40)
   
   public var calculatedTableViewHeight: CGFloat {
      let height = models
         .compactMap { ($0 as? CellTypable)?.cellType.prototype.cellHeight }
         .reduce(0) { $0 + $1 }
      
      return height
   }
   
   public var actionSelfClosure: DefaultSelfClosure?
   
   open override func viewDidLoad() {
      super.viewDidLoad()
      
      tableView = BaseTableView(frame: view.bounds, style: .plain)
      tableView.autoresizingMask = UIView.flexible
      tableView.delegate = self
      tableView.dataSource = self
      tableView.tableFooterView = UIView()
      
      tableView.estimatedRowHeight = 0
      tableView.estimatedSectionHeaderHeight = 0
      tableView.estimatedSectionFooterHeight = 0
      
      view.addSubview(tableView)
      
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
   }
   
   open func reloadData() {
      tableView.reloadData()
   }
   
   open func reloadDataAnimated(_ animated: Bool = true) {
      if animated {
         UIView.transition(with: tableView,
                           duration: 0.2,
                           options: .transitionCrossDissolve,
                           animations: {
                              self.tableView.reloadData() },
                           completion: nil)
      } else {
         reloadData()
      }
   }
   
   open func reloadInPlace(animated: Bool = false) {
      let contentOffset = tableView.contentOffset
      
      if animated {
         UIView.transition(with: tableView,
                           duration: 0.2,
                           options: .transitionCrossDissolve,
                           animations: {
                              self.tableView.reloadData()
                              self.tableView.setContentOffset(contentOffset, animated: false)
         },
                           completion: nil)
      } else {
         reloadData()
      }
      
      tableView.setContentOffset(contentOffset, animated: false)
   }
   
   open override var shouldAutorotate: Bool {
      return false
   }
   
   open func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 0
   }
   
   open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      return UITableViewCell()
   }
   
   open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      if section < headerTitles.count {
         if headerTitles[section] != nil {
            return headerParameters.height
         } else {
            return 0
         }
      } else {
         return 0
      }
   }
   
   open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      if section < headerTitles.count {
         if let title = headerTitles[section] {
            return TableSectionHeaderView(text: title, height: headerParameters.height, backColor: headerParameters.backColor, fontColor: headerParameters.fontColor)
         } else {
            return nil
         }
      } else {
         return nil
      }
   }
   
   open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 0
   }
   
   open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.tableView.deselectRow(at: indexPath, animated: true)
      
      let model = models[indexPath.row]
      if let model = model as? ActionClosureProtocol {
         model.actionClosure?()
      } else {
         if let model = model as? ActionSelfClosureProtocol {
            model.actionSelfClosure?(model)
         } else {
            if actionSelfClosure != nil {
               actionSelfClosure?(model)
            } else {
               if let cell = tableView.cellForRow(at: indexPath) as? BaseCellProtocol {
                  cell.actionClosure?()
               }
            }
         }
      }
   }
}

////////////////////////
extension BaseTC {
   open func setTableViewHeader(height: CGFloat) {
      let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: height))
      tableView.tableHeaderView = headerView
   }
   
   open func setTableViewFooter(height: CGFloat) {
      let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: height))
      tableView.tableFooterView = footerView
   }
}

extension BaseTC {
   public func deleteRows(indexPaths: [IndexPath], animated: Bool = false) {
      if animated {
         tableView.beginUpdates()
         tableView.deleteRows(at: indexPaths, with: .fade)
         tableView.endUpdates()
      } else {
         tableView.deleteRows(at: indexPaths, with: .automatic)
         tableView.reloadData()
      }
   }
   
   public func moveRows(indexPaths: [IndexPath], toSection: Int, animated: Bool = false) {
      let toIndexPath = IndexPath(row: 0, section: toSection)
      if animated && indexPaths.count == 1 {
         tableView.beginUpdates()
         tableView.moveRow(at: indexPaths.first!, to: toIndexPath)
         tableView.endUpdates()
      } else {
         tableView.reloadData()
      }
   }
}

extension BaseTC {
}
