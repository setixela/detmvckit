//
//  MultiSectionsTCS.swift
//  Invercity
//
//  Created by AlexSolo on 24.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

/////
open class BaseMSTCS<C: EnumCells>: BaseTCS<C>, MultiSectionsProtocol {
   public var modelsArray: [[Contentable]] = [[]]
   
   override open func cleanAllData() {
      super.cleanAllData()
      modelsArray = [[]]
   }
   
   override open func numberOfSections(in tableView: UITableView) -> Int {
      return modelsArray.count
   }
   override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let models = modelsArray[section]
      if section == 0 {
         if isSearchActive {
            return filteredModels.count
         }
      }
      let count = models.count
      return count
   }
   override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let section = indexPath.section
      let row = indexPath.row
      
      let models = modelsArray[section]
      var model: Contentable
      
      if section == 0 && isSearchActive {
         model = filteredModels[row]
      } else {
         model = models[row]
      }
      
      return heightFor(model: model, indexPath: indexPath)
   }
   override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let section = indexPath.section
      let row = indexPath.row
      
      var models: [Contentable]
      if isSearchActive == true && section == 0 {
         models = filteredModels
      } else {
         models = self.modelsArray[section]
      }
      let model = models[row]
      
      return cellFor(model: model, indexPath: indexPath)
   }
}
