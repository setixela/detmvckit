//
//  BaseTCD+BaseTCS.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 09.02.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

//////// USAGE

public protocol ModelsInjection {
   func setModels(_ models: [JModel & CellTypable], withCellType cellType: EnumCells)
}

extension ModelsInjection where Self: DataControllable {
   public func setModels(_ newModels: [JModel & CellTypable], withCellType cellType: EnumCells) {
      if newModels.count > 0 {
         self.models = newModels.map { $0.cellType = cellType; return $0 }
      }
   }
}

////////////////////////////////////////////////

open class BaseTCD<CT: EnumCells>: BaseTC, DataControllable {
   public var cellReuseIdentifier: String = "Cell"
   public var cellPrototype: BaseCellProtocol = BaseCell()
   private var registeredCellTypes: [CT] = []
   
   public var dataView: DataViewable {
      return self.tableView
   }
   
   private var isTableviewAnimating = false
   
   // MARK: -------------------------------------- methods controllable
   
   open func cleanAllData() {
      self.models = []
   }
   
   open override func viewDidLoad() {
      super.viewDidLoad()
      
      self.setupTableView()
   }
   
   open func setupTableView() {
      //    fatalError("You need override setupTableView method")
   }
   
   public func updateCell(indexPath: IndexPath, model: Contentable? = nil) {
      if model != nil {
         models[indexPath.row] = model!
      }
      
      self.tableView.beginUpdates()
      self.tableView.reloadRows(at: [indexPath], with: .fade) //try other animations
      self.tableView.endUpdates()
   }
   
   open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let count = models.count
      return count
   }
   
   open override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let model = self.models[indexPath.row]
      return self.heightFor(model: model, indexPath: indexPath)
   }
   
   public func heightFor(model: Contentable, indexPath: IndexPath) -> CGFloat {
      var height: CGFloat
      
      if let type = (model as? CellTypable)?.cellType {
         if let cellPrototype = type.prototype as? DynamicHeightable {
            height = cellPrototype.dynamicHeight(model: model)
         } else {
            let cellPrototype = type.prototype
            height = cellPrototype.cellHeight
         }
         
      } else {
         if let cell = cellPrototype as? DynamicHeightable {
            height = cell.dynamicHeight(model: model)
         } else {
            let classOfCell = type(of: cellPrototype)
            height = classOfCell.kHeight()
         }
      }
      
      if let subModel = (model as? StaticCellModel)?.subModel {
         if let type = (model as? CellTypable)?.cellType {
            if let cellPrototype = type.prototype as? DynamicHeightable {
               height = cellPrototype.dynamicHeight(model: model)
            } else {
               // height = self.cellPrototype.cellHeight
            }
            
         } else {
            if let type = (subModel as? CellTypable)?.cellType {
               if let cellPrototype = type.prototype as? DynamicHeightable {
                  height = cellPrototype.dynamicHeight(model: subModel)
               } else {
                  //   height = self.cellPrototype.cellHeight
               }
            }
         }
      }
      
      return height
   }
   
   open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let model = self.models[indexPath.row]
      
      if let vc = self as? Paginable {
         vc.checkAndLoadNextPage(models: models, indexPath: indexPath)
      }
      
      return self.cellFor(model: model, indexPath: indexPath)
   }
   
   // MARK: func cellFor(model: Contentable, indexPath: IndexPath) -> UITableViewCell
   
   open func cellFor(model: Contentable, indexPath: IndexPath) -> UITableViewCell {
      var cell: BaseCellProtocol
      var cellType: CT?
      
      print("A")
      if self is ModelsInjection {
         print("B")
         if let typeCell = (model as? CellTypable)?.cellType as? CT {
            cellType = typeCell
            let prototype = typeCell.prototype
            let className: String = String(describing: type(of: prototype))
            let types = type(of: prototype)
            self.tableView.register(types, forCellReuseIdentifier: className)
            cell = (self.tableView.dequeueReusableCell(withIdentifier: className, for: indexPath) as? BaseCellProtocol)!
            
         } else {
            fatalError("Error! Please add cellType to model")
         }
      } else {
         if let type1 = (model as? CellTypable)?.cellType as? CT {
            cellType = type1
            cell = (dequeueReusableCell(cellType: type1, forIndexPath: indexPath) as? BaseCellProtocol)!
         } else {
            cellType = self.registeredCellTypes.first
            cell = (self.tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier, for: indexPath) as? BaseCellProtocol)!
         }
      }
      cell.actionDelegate = self
      
      if model is StaticCellModel {
         if let subModel = (model as? StaticCellModel)?.subModel {
            if let slf = self as? CellBindManagerable,
               let cellType = cellType {
               slf.cellBindManager.bind(cellType: cellType, cell: cell, model: subModel)
               return (cell as? UITableViewCell) ?? UITableViewCell()
            } else {
               return (cell.bind(subModel) as? UITableViewCell) ?? UITableViewCell()
            }
         }
      }
      
      cell.indexPath = indexPath
      if let slf = self as? CellBindManagerable,
         let cellType = cellType {
         slf.cellBindManager.bind(cellType: cellType, cell: cell, model: model)
         return (cell as? UITableViewCell) ?? UITableViewCell()
      } else {
         return (cell.bind(model) as? UITableViewCell) ?? UITableViewCell()
      }
   }
   
   /////////////
   
   //   public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //      if let model = models[indexPath.row] as? StaticCellModel {
   //         model.actionClosure?()
   //      } else {
   //         if let cell = tableView.cellForRow(at: indexPath) as? BaseCell {
   //            cell.actionClosure?()
   //         }
   //      }
   //   }
   
   //// ContentMarkable
   public func markedModelsCount() -> Int {
      var count = 0
      for content in models where content is ContentMarkable {
         if var model = content as? ContentMarkable {
            if model.isMarked {
               count += 1
            }
         }
      }
      return count
   }
   
   public func markAllModels(isMarked: Bool) {
      for content in models where content is ContentMarkable {
         if var model = content as? ContentMarkable {
            model.isMarked = isMarked
         }
      }
   }
   
   public func markedModels(isMarked: Bool) -> [Contentable] {
      return models.filter { (content) -> Bool in
         if content is ContentMarkable {
            if var model = content as? ContentMarkable {
               if isMarked {
                  return model.isMarked
               } else {
                  return !model.isMarked
               }
            } else {
               return false
            }
         } else {
            return false
         }
      }
   }
   
   public func markedIndexPaths(isMarked: Bool, section: Int) -> [IndexPath] {
      var indexPaths: [IndexPath] = []
      for (idx, content) in models.enumerated() where content is ContentMarkable {
         if let model = content as? ContentMarkable {
            if isMarked {
               if model.isMarked {
                  let indexPath = IndexPath(row: idx, section: section)
                  indexPaths.append(indexPath)
               }
            } else {
               if !model.isMarked {
                  let indexPath = IndexPath(row: idx, section: section)
                  indexPaths.append(indexPath)
               }
            }
         }
      }
      return indexPaths
   }
}

extension BaseTCD {
   /////
   public func registerNib(cellType: CT) {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      self.tableView.register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
      cellPrototype = prototype
      registeredCellTypes.append(cellType)
   }
   
   public func registerNib(prototype: BaseCellProtocol) {
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      self.tableView.register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
      cellPrototype = prototype
   }
   
   public func registerClass(prototype: BaseCellProtocol) {
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      self.tableView.register(type(of: (prototype as AnyObject)), forCellReuseIdentifier: className)
      cellPrototype = prototype
   }
   
   public func registerClass(cellType: CT) {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      cellReuseIdentifier = className
      let types = type(of: prototype)
      self.tableView.register(types, forCellReuseIdentifier: className)
      cellPrototype = prototype
      registeredCellTypes.append(cellType)
   }
   
   public func registerClasses(cellTypes: [CT]) {
      for type in cellTypes {
         self.registerClass(cellType: type)
         self.registeredCellTypes.append(type)
      }
   }
   
   public func dequeueReusableNibPrototype(className: String, forIndexPath: IndexPath) -> UITableViewCell {
      let cell = self.tableView.dequeueReusableCell(withIdentifier: className, for: forIndexPath)
      
      return cell
   }
   
   public func dequeueReusableCell(cellType: CT, forIndexPath: IndexPath) -> UITableViewCell {
      let prototype = cellType.prototype
      let className: String = String(describing: type(of: prototype))
      let cell = self.tableView.dequeueReusableCell(withIdentifier: className, for: forIndexPath)
      
      return cell
   }
}
