//
//  BaseVC.swift
//  GdeDuet2
//
//  Created by Admin on 27.04.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import UIKit

open class BaseVC: UIViewController, NetDataChainReload, ActionDelegate {
   public var isNeedReloadWhenAppear: Bool = false
   
   open func performAction(object: ActionObject) {
      print("override perform")
   }
   
   open func perform(_ action: EnumActions) {
      print("override perform")
   }
   
   public var isShiftScreenMode = false // keyboard protocol needed
   
   open func reloadNetData(animated: Bool = true) {
      print("Override RELOAD NET DATA!")
   }
   
   var enablePanGesture = true
   
   override open func viewDidLoad() {
      
      super.viewDidLoad()
      
      if enablePanGesture {
         //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
      }
      
      self.view.backgroundColor = .white
      let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 19), NSAttributedString.Key.foregroundColor: UIColor.white] //change size as per your need here.
      self.navigationController?.navigationBar.titleTextAttributes = attributes
      
      setNeedsStatusBarAppearanceUpdate()
   }
   
   override open func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      
      self.dismissKeyboard()
   }
   
   override open func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      if isNeedReloadWhenAppear {
         self.reloadNetData()
      }
      
      isNeedReloadWhenAppear = false
   }
   
   override open var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
   }
   
   override open var prefersStatusBarHidden: Bool {
      return false
   }
}
