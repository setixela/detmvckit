//
//  UITableViewCellExtension.swift
//  GdeDuet2
//
//  Created by AlexSolo on 24.04.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public extension UITableViewCell {
   var cV: UIView {
      return contentView
   }
}
