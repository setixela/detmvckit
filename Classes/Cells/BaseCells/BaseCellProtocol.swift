//
//  BaseCellProtocol.swift
//  Invercity
//
//  Created by AlexSolo on 24.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import UIKit

public protocol BaseCellProtocol: Configurable, CellTouchable, CellBindable, ActionSender, IndexPathable {
   static func kHeight() -> CGFloat
   func bind(_ model: Contentable) -> Self
   func markModel(_ isMarked: Bool)
   
   var touchCoord: CGPoint { get set }
   var model: Contentable? { get set }
   
   var actionDelegate: ActionDelegate? { get set } // weak
   var cellHeight: CGFloat { get }
   
   var actionClosure: (() -> Void)? { get set }
}

public extension BaseCellProtocol {
   public func markModel(_ isMarked: Bool) {
      guard model != nil, model is ContentMarkable else { return }
      
      if var content = self.model as? ContentMarkable {
         content.isMarked = isMarked
      }
   }
}

public protocol IndexPathable {
   var indexPath: IndexPath? { get set }
}

open class BaseCell: UITableViewCell, BaseCellProtocol {
   public var indexPath: IndexPath?
   
   open func bind(_ model: Contentable) -> Self {
      print("Override bind!")
      return self
   }
   
   public var touchCoord: CGPoint = CGPoint.zero
   public var model: Contentable?
   
   public weak var actionDelegate: ActionDelegate?
   
   open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      if let touch = touches.first {
         let point = touch.location(in: self)
         self.touchCoord = point
      }
      super.touchesBegan(touches, with: event)
   }
   
   public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      self.configure()
   }
   
   public required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
   
   ////////////////////////
   
   open class func kHeight() -> CGFloat {
      return 48
   }
   
   public var cellHeight: CGFloat {
      return type(of: self).kHeight()
   }
   
   public var actionClosure: (() -> Void)?
   
   open func configure() {
      // print("BaseCell configuring...")
      self.backgroundColor = UIColor.white
      self.clipsToBounds = true
      self.contentView.clipsToBounds = true
      self.separatorInset = .zero
   }
   
   public func markModel(_ isMarked: Bool) {
      guard self.model != nil, model is ContentMarkable else { return }
      
      if var content = self.model as? ContentMarkable {
         content.isMarked = isMarked
      }
   }
}
