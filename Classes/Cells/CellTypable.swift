//
//  CellTypableProtocol.swift
//  GdeDuet2
//
//  Created by AlexSolo on 24.03.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//
import Foundation

public protocol EnumCells {
   var prototype: BaseCellProtocol { get }
}

public protocol CellBinderManagerProtocol {
   func bind(cellType: EnumCells, cell: BaseCellProtocol, model: Contentable)
}

extension CellBinderManagerProtocol {
   public func bind(cell: BaseCellProtocol, model: Contentable) {
      _ = cell.bind(model)
   }
}

public protocol CellBindManagerable {
   var cellBindManager: CellBinderManagerProtocol { get set }
}

public protocol CellTypable: class {
   var cellType: EnumCells! { get set }
}
