import Foundation

public protocol SubModellable: class {
   var subModel: Contentable? {get set}
}

open class StaticCellModel: CellTypable, SubModellable, Contentable, ActionClosureProtocol {
   
   public var cellType: EnumCells!
   public var subModel: Contentable?
   
   public var actionClosure: DefaultClosure?
   
   public required init(model: Contentable? = nil, cellType: EnumCells, actionClosure: DefaultClosure? = nil) {
      self.subModel = model
      self.cellType = cellType
      self.actionClosure = actionClosure
   }
}
