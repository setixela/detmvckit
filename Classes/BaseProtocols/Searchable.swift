//
//  Searchable.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 11.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import SetixelaUtils
import UIKit

// MARK: -------------------------------------- Searchable protocols

public protocol Searchable: UISearchBarDelegate, SearchControllerDelegate {
   
   var searchController: SearchController! { get set }
   var filteredModels: [Contentable] { get }
   
   var isSearchActive: Bool { get }
}

extension Searchable {
   public var isSearchActive: Bool {
      if let sc = searchController {
         let isActive = sc.customSearchBar.isFirstResponder
         return (isActive && sc.customSearchBar.text != "")
      } else {
         return false
      }
   }
}
