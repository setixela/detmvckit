//
//  Paginable.swift
//  GdeDuet2
//
//  Created by setiXela on 24/04/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public struct Pages {
   public var isExponentPerPage: Bool = false
	public var isPageLoading: Bool = false
	public var currentPage: Int = 1
	public var perPage: Int = 25
	public var isFinished: Bool = false
	
	public init(perPage: Int) {
		self.init()
		self.perPage = perPage
	}
	
	public init() {
		self.isPageLoading = false
		self.currentPage = 1
		self.perPage = 25
		self.isFinished = false
	}
	
	// this method need to reload all data to current page
	public mutating func recalculate() {
		if (self.currentPage)>2 {
			self.perPage = (self.currentPage-1)*self.perPage
		}
		self.currentPage = 1
	}
}

public protocol Paginable {
	var pages: Pages {get set}
	func loadNextPage()
  // func loadNextPage(completion: ([Contentable])->()?)
}

extension Paginable {
	public func checkAndLoadNextPage(models: [Contentable], indexPath: IndexPath) {
		if (indexPath.row) >= (models.count-7) && !self.pages.isPageLoading && !self.pages.isFinished {
			loadNextPage()
		}
	}
   
   public func loadNextPage() {
      print("Implement loadNextPage")
   }
   
//   func loadNextPage(completion: ([Contentable])->()?) {
//      fatalError("Implement loadNextPage(completion: (_ resultModels: [Contentable])->()) ")
//   }
}
