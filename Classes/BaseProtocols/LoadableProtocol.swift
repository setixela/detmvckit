//
//  LoadableView.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 03.11.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

public class ActivityView: UIView, Configurable {
   var activityIndicator: UIActivityIndicatorView?
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      
      self.configure()
   }
   
   public required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
   
   public func configure() {
      self.backgroundColor = UIColor.black.withAlphaComponent(0.15)
      
      self.activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
      self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      self.activityIndicator?.hidesWhenStopped = true
      
      self.addSubview(self.activityIndicator!)
      self.activityIndicator?.center = CGPoint(x: self.width / 2, y: self.height / 2)
   }
}

public protocol LoadableCustom: class {
   var activityView: ActivityView? { get set }
}

extension LoadableCustom where Self: UIView {
   public func startLoadingIndicator() {
      if self.activityView == nil {
         let frame = self.bounds
         self.activityView = ActivityView(frame: frame)
         self.activityView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         self.activityView?.activityIndicator?.hidesWhenStopped = true
         
         self.addSubview(self.activityView!)
      }
      
      self.activityView?.frame = self.bounds
      
      self.startLoading()
   }
   
   private func startLoading() {
      self.bringSubviewToFront(self.activityView!)
      self.activityView?.activityIndicator?.center = CGPoint(x: activityView!.width / 2, y: activityView!.height / 2)
      
      self.activityView!.isHidden = false
      self.activityView!.activityIndicator?.startAnimating()
      
      self.setNeedsDisplay()
   }
   
   public func stopLoadingIndicator() {
      if self.activityView != nil {
         self.activityView!.activityIndicator?.stopAnimating()
         self.activityView!.isHidden = true
      }
      
      self.setNeedsDisplay()
   }
}

// MARK: -------------------------------------- loadable protocol

public protocol Loadable: class {
   var activityIndicator: UIActivityIndicatorView? { get set }
}

extension Loadable where Self: UIView {
   public func startLoadingIndicator(isGrayBack: Bool = false) {
      if self.activityIndicator == nil {
         self.activityIndicator = UIActivityIndicatorView(style: .gray)
         self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         
         self.activityIndicator?.hidesWhenStopped = true
         
         if let slf = self as? UITableViewCell {
            slf.cV.addSubview(self.activityIndicator!)
         } else {
            self.addSubview(self.activityIndicator!)
         }
      }
      if isGrayBack {
         self.backgroundColor = UIColor.lightGray
      }
      self.startLoading()
   }
   
   public func startMediumLoadingIndicator(isGrayBack: Bool = false) {
      if self.activityIndicator == nil {
         self.activityIndicator = UIActivityIndicatorView(style: .gray)
         self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         self.activityIndicator?.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
         
         self.activityIndicator?.hidesWhenStopped = true
         if let slf = self as? UITableViewCell {
            slf.cV.addSubview(self.activityIndicator!)
         } else {
            self.addSubview(self.activityIndicator!)
         }
      }
      if isGrayBack { self.backgroundColor = UIColor.lightGray }
      self.startLoading()
   }
   
   public func startBigLoadingIndicator(isGrayBack: Bool = false) {
      if self.activityIndicator == nil {
         self.activityIndicator = UIActivityIndicatorView(style: .gray)
         self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         self.activityIndicator?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
         
         self.activityIndicator?.hidesWhenStopped = true
         if let slf = self as? UITableViewCell {
            slf.cV.addSubview(self.activityIndicator!)
         } else {
            self.addSubview(self.activityIndicator!)
         }
      }
      if isGrayBack { self.backgroundColor = UIColor.lightGray }
      self.startLoading()
   }
   
   public func startWhiteLoadingIndicator() {
      if self.activityIndicator == nil {
         self.activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
         self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         
         self.activityIndicator?.hidesWhenStopped = true
         if let slf = self as? UITableViewCell {
            slf.cV.addSubview(self.activityIndicator!)
         } else {
            self.addSubview(self.activityIndicator!)
         }
      }
      
      self.startLoading()
   }
   
   public func startWhiteLoadingIndicator(scale: CGFloat = 1.0) {
      if self.activityIndicator == nil {
         self.activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
         self.activityIndicator?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
         self.activityIndicator?.transform = CGAffineTransform(scaleX: scale, y: scale)
         
         self.activityIndicator?.hidesWhenStopped = true
         self.addSubview(self.activityIndicator!)
      }
      
      self.startLoading()
   }
   
   private func startLoading() {
      if let slf = self as? UITableViewCell {
         slf.cV.bringSubviewToFront(self.activityIndicator!)
         self.activityIndicator?.center = CGPoint(x: slf.cV.width / 2, y: slf.cV.height / 2)
      } else {
         self.bringSubviewToFront(self.activityIndicator!)
         self.activityIndicator?.center = CGPoint(x: self.width / 2, y: self.height / 2)
      }
      
      self.activityIndicator?.startAnimating()
   }
   
   public func stopLoadingIndicator() {
      if self.activityIndicator != nil {
         self.activityIndicator?.stopAnimating()
      }
   }
}
