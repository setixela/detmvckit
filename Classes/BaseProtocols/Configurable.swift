//
//  Configurable.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 05.12.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation

public protocol Configurable {
   func configure()
}
