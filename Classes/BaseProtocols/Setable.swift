//
//  Setable.swift
//  SetixelaMVCKit
//
//  Created by AlexSolo on 15.01.2018.
//  Copyright © 2018 Gaozhiyuan. All rights reserved.
//

import Foundation

public protocol Setable {
   func set(model: Contentable)
}
