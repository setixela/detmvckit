//
//  ContentMarkable.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 13.04.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import UIKit

public protocol ContentMarkable {
   var isMarked: Bool {get set}
}
