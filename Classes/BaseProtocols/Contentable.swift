//
//  Contentable.swift
//  Invercity
//
//  Created by AlexSolo on 24.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

///////////////////////////////////////////////////////////////////////////////////////////
public protocol Contentable {
   
}

extension Int: Contentable {}
extension String: Contentable {}
