//
//  ViewKeyboardShiftProtocol.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 01.06.2018.
//  Copyright © 2018 Gaozhiyuan. All rights reserved.
//

import Foundation
import SetixelaUtils

public protocol KeyboardProtocol: class {
   var keyboardTop: CGFloat { get }
}

extension UIViewController {
   public func startKeyboardListening() {
      NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
   }
   
   public func stopKeyboardListening() {
      NotificationCenter.default.removeObserver(self)
   }
   
   @objc open func keyboardDidShow(_ notification: NSNotification) {
      if let keyboardFrame: CGRect = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
         let keyboardTop = screenHeight - keyboardFrame.height
         
         UIView.animate(withDuration: 0.25) {
            self.view.scaleBottomToY(y: keyboardTop)
         }
      }
   }
   
   @objc open func keyboardDidHide(_ notification: NSNotification) {
      let keyboardTop: CGFloat = screenHeight
      
      UIView.animate(withDuration: 0.25) {
         self.view.scaleBottomToY(y: keyboardTop)
      }
   }
}
