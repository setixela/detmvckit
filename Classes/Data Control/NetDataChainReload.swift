//
//  NetDataReload.swift
//  GdeDuet2
//
//  Created by setiXela on 07/05/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public protocol NetDataChainReload {

   var isNeedReloadWhenAppear: Bool {get set}
	func reloadNetData(animated: Bool)
	
	func initReloadDataChain(animated: Bool)
   func reloadDataChainFor(nc: UINavigationController?, animated: Bool)
   func setChainReloadNetDataWhenAppears(_ isNeed: Bool)
}

public extension NetDataChainReload where Self: UIViewController {
	
  public  func initReloadDataChain(animated: Bool = true) {
		//print(self.navigationController?.title)
		if let nc = self.navigationController {
			
			for viewController in nc.viewControllers {
				if let vc = viewController as? NetDataChainReload {
					vc.reloadNetData(animated: animated)
				}
			}
		}
	}
   
   public func reloadDataChainFor(nc: UINavigationController?, animated: Bool = true) {
      if let nc = nc {
         for viewController in nc.viewControllers {
            if let vc = viewController as? NetDataChainReload {
               vc.reloadNetData(animated: animated)
            }
         }
      }
   }
   
   public func setChainReloadNetDataWhenAppears(_ isNeed: Bool) {
      if let nc = self.navigationController {
         
         for viewController in nc.viewControllers {
            if var vc = viewController as? NetDataChainReload {
               vc.isNeedReloadWhenAppear = isNeed
            }
         }
      }
   }
}
