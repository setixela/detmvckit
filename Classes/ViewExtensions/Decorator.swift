//
//  Decorator.swift
//  GdeDuet2
//
//  Created by setiXela on 09/05/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import UIKit

public typealias Decoration<T> = (T) -> Void

public struct Decorator<T> {
	let object: T
	public func apply(_ decorations: Decoration<T>...) {
		decorations.forEach({ $0(object) })
	}
}

public protocol DecoratorCompatible {
	associatedtype DecoratorCompatibleType
	var decorator: Decorator<DecoratorCompatibleType> {get}
}

public extension DecoratorCompatible {
	var decorator: Decorator<Self> {
		return Decorator(object: self)
	}
}

//extension UILabel: DecoratorCompatible {}

extension UIView: DecoratorCompatible {}

//extension UITableView: DecoratorCompatible {}
