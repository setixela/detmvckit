//
//  BaseView.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 12.10.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import UIKit

open class BaseView: UIView, Configurable {

   override public init(frame: CGRect) {
      super.init(frame: frame)
      
      self.configure()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.configure()
   }
   
   open func configure() {
      print("Override configure!!!!")
   }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
