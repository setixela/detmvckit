//
//  EngineUploadExt.swift
//  SetixelaMVCKit
//
//  Created by Aleksandr Solovyev on 07/11/2018.
//  Copyright © 2018 Gaozhiyuan. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SetixelaUtils

extension Engine {
   public func uploadImage<T>(data: Data,
                              microService: MicroServices,
                              apiMethod: String,
                              parameters: Parameters?,
                              isToken: Bool) -> Promise<T> where T: ApiResultProtocol {
      return Promise { seal in
         
         guard isInternetAvailable() else {
            seal.reject(ApiPromiseError.noInternet)
            return }
         
         guard let str = microService.rawValue as? String else {
            seal.reject(ApiPromiseError.microServicesEnumRaw)
            return
         }
         
         var url = str + "/" + apiMethod
         
         if apiSettings.httpScheme == .https {
            url = "https://" + url
         } else {
            url = "http://" + url
         }
         
         print("\n\nREQUEST: ///////////////////////////////////////////////////////")
         
         var headers: HTTPHeaders? = [:]
         
         if isToken {
            print("TOKEN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if let token = token {
               headers = [apiSettings.tokenHeader: apiSettings.tokenPrefix + token]
               print(headers!.description)
            } else {
               seal.reject(ApiPromiseError.noToken)
               print("NO TOKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
               return
            }
         }
         
         headers?["Content-Type"] = "multipart/form-data"
         print(headers?.description)
         
         if parameters != nil {
            print("PARAMETERS: ####################################################")
            print(parameters!.description)
         }
         
         Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: "file_avatar", fileName: "file_avatar", mimeType: "image/jpg")
         }, usingThreshold: 0, to: url, method: .post, headers: headers) { result in
            switch result {
            case .success(let upload, _, _):
               
               upload.uploadProgress { progress in
                  print("Upload Progress: \(progress.fractionCompleted)")
               }
               
               upload.responseJSON { response in
                  print(response.result.value)
                  
                  let result = response.result
                  let value = result.value as? JSON
                  let json = value?[self.dataKey] as? JSON
                  
                  let fullFill = T(value: json, errorInt: 200)
                  seal.fulfill(fullFill)
               }
               
            case .failure(let encodingError):
               print(encodingError)
               seal.reject(ApiPromiseError.unknown(encodingError))
            }
         }
      }
   }
}
