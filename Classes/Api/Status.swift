//
//  Status.swift
//  Invercity
//
//  Created by AlexSolo on 02.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

typealias StatusType = [Int: String]
