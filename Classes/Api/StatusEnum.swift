//
//  StatusEnum.swift
//  Invercity
//
//  Created by AlexSolo on 07.06.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

//
public enum Status: String {
   case
   success,
   invalidEmail,
   invalidToken,
   badCredentials,
   notFound,
   serverError,
   badRequest,
   accessDenied,
   
   unknown,
   serializationError
}
