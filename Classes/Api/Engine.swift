//
//  Engine.swift
//  Invercity
//
//  Created by AlexSolo on 02.02.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Alamofire
import Foundation
import FunctionalSwift
import PromiseKit
import SetixelaUtils

extension Error {
   public func promiseErrorDescription() -> String {
      var errorDescr = "Unknown error type"
      if let error = self as? ApiPromiseError {
         errorDescr = error.localizedDescription
      } else {
         errorDescr = self.localizedDescription
      }
      
      return errorDescr
   }
}

public protocol ApiMethodProtocol { // }: Hashable {
   func setup() -> ApiRequest
   
   var newSettings: ApiRequestSettings { get }
   func createRequest(settings: ApiRequestSettings) -> ApiRequest
}

public struct ApiRequestSettings {
   public var params: Parameters?
   public var method: String = ""
   public var httpMethod: HTTPMethod = .get
   public var isToken: Bool = true
   public var arrayKey: String?
}

public extension ApiMethodProtocol {
   public var newSettings: ApiRequestSettings {
      return ApiRequestSettings()
   }
   
   public func createRequest(settings: ApiRequestSettings) -> ApiRequest {
      let apirequest = ApiRequest(apiMethod: settings.method, httpMethod: settings.httpMethod, parameters: settings.params, isToken: settings.isToken, arrayKey: settings.arrayKey)
      return apirequest
   }
}

public protocol ApiErrorTypeProtocol: Error, RawRepresentable where Self.RawValue == Int {}

public protocol BaseObject {}

extension Array: BaseObject {}
extension Dictionary: BaseObject {}

public struct ApiRequest {
   public var apiMethod: String
   public var httpMethod: HTTPMethod
   public var parameters: Parameters?
   public var isToken: Bool
   public var arrayKey: String?
   
   public init(apiMethod: String, httpMethod: HTTPMethod, parameters: Parameters?, isToken: Bool, arrayKey: String? = nil) {
      self.apiMethod = apiMethod
      self.httpMethod = httpMethod
      self.parameters = parameters
      self.isToken = isToken
      self.arrayKey = arrayKey
   }
}

public protocol ApiResultProtocol: Error {
   associatedtype J: JSONModelProtocol
   associatedtype T: ApiErrorTypeProtocol
   var value: J? { get set }
   var arrayValue: [J]? { get set }
   var errorInt: Int { get set }
   var errorType: T? { get }
   init(value: JSON?, errorInt: Int)
}

open class ApiResultBase<T: ApiErrorTypeProtocol, J: JSONModelProtocol>: ApiResultProtocol {
   public required init(value: JSON?, errorInt: Int) {
      self.value = J(json: value)
      self.errorInt = errorInt
   }
   
   public var value: J?
   public var arrayValue: [J]?
   public var errorInt: Int
   
   public var errorType: T? {
      return T(rawValue: self.errorInt)
   }
}

public typealias Completion = (_ success: Bool, _ object: BaseObject?) -> Void
public typealias CompletionJson = (_ success: Bool, _ object: JSON?) -> Void

public protocol EngineProtocol {
   associatedtype MicroServices: RawRepresentable
   
   var apiSettings: ApiSettingsProtocol { get }
   func request<T>(_ apiMethod: ApiMethodProtocol, microsService: MicroServices) -> Promise<T> where T: ApiResultProtocol
   func request<T>(microService: MicroServices,
                   apiMethod: String,
                   method: HTTPMethod,
                   parameters: Parameters?,
                   isToken: Bool,
                   arrayKey: String?) -> Promise<T> where T: ApiResultProtocol
   
   func uploadFileImage(urlString: String, image: UIImage, isToken: Bool, parameters: JSON?, completion: @escaping (_ response: Response?) -> Void)
   
   var token: String? { get set }
}

public class Engine<MicroServices: RawRepresentable>: EngineProtocol {
   public var token: String?
   
   public var apiSettings: ApiSettingsProtocol
   public var dataKey: String
   public var resultCodeKey: String
   
   public required init(apiSettings: ApiSettingsProtocol, dataKey: String, resultCodeKey: String) {
      self.apiSettings = apiSettings
      self.dataKey = dataKey
      self.resultCodeKey = resultCodeKey
   }
}

///////////////////////////////////////////////
public extension Engine {
   public func request<T>(_ apiMethod: ApiMethodProtocol, microsService: MicroServices) -> Promise<T> where T: ApiResultProtocol {
      let request = apiMethod.setup()
      return
         self.request(microService: microsService,
                      apiMethod: request.apiMethod,
                      method: request.httpMethod,
                      parameters: request.parameters,
                      isToken: request.isToken,
                      arrayKey: request.arrayKey)
   }
   
   public func request<T>(microService: MicroServices,
                          apiMethod: String,
                          method: HTTPMethod,
                          parameters: Parameters?,
                          isToken: Bool,
                          arrayKey: String? = nil) -> Promise<T> where T: ApiResultProtocol {
      return Promise { seal in
         
         guard isInternetAvailable() else {
            seal.reject(ApiPromiseError.noInternet)
            return }
         
         guard let str = microService.rawValue as? String else {
            seal.reject(ApiPromiseError.microServicesEnumRaw)
            return
         }
         
         var url = str + "/" + apiMethod
         
         if apiSettings.httpScheme == .https {
            url = "https://" + url
         } else {
            url = "http://" + url
         }
         
         var parameters = parameters
         
         print("\n\nREQUEST: ///////////////////////////////////////////////////////")
         
         var headers: HTTPHeaders?
         
         if isToken {
            print("TOKEN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if let token = token {
               headers = [apiSettings.tokenHeader: apiSettings.tokenPrefix + token]
               print(headers!.description)
            } else {
               seal.reject(ApiPromiseError.noToken)
               print("NO TOKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
               return
            }
         }
         
         if parameters != nil {
            print("PARAMETERS: ####################################################")
            print(parameters!.description)
         }
         
         if parameters != nil && method == .get {
            var paramsString = ""
            if parameters != nil {
               paramsString = "?"
               parameters?.forEach { dic in
                  
                  if paramsString != "?" {
                     paramsString += "&"
                  }
                  
                  let key = dic.key
                  let value = dic.value
                  if let keyValueStr = (key + "=" + "\(value)").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) {
                     paramsString += keyValueStr
                  } else {
                     seal.reject(ApiPromiseError.paramsKeyValue)
                     return
                  }
               }
            }
            
            url += paramsString
            parameters = nil
         }
         
         print("URL: (\(url))", ", METHOD:", method.rawValue)
         
         url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? url
         
         Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
            print("RESULT: ++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            
            switch response.result {
            case .success:
               let result = response.result
               let value = result.value as? JSON
               let json = value?[self.dataKey] as? JSON
               var apiResult = T(value: json, errorInt: value?[self.resultCodeKey] as? Int ?? 0)
               if let arrayKey = arrayKey {
                  if let array = json?[arrayKey] as? [JSON] {
                     print(array)
                     apiResult.arrayValue = array.map {
                        T.J(json: $0)
                     }
                     print(apiResult)
                  }
               }
               
               seal.fulfill(apiResult)
               
               debugPrint(response.result)
               
            case .failure(let error):
               
               seal.reject(ApiPromiseError.unknown(error))
               debugPrint("Request failed with error: \(error)")
            }
            
            print("END: -----------------------------------------------------------\n\n")
         }
      }
   }
}

extension Engine {
   public func uploadFileImage(urlString: String, image: UIImage, isToken: Bool, parameters: JSON?, completion: @escaping (_ response: Response?) -> Void) {
      guard let data = image.jpegData(compressionQuality: 0.65) else { completion(nil); return }
      
      var headers: HTTPHeaders?
      
      if isToken {
         print("TOKEN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
         if let token = token {
            headers = [apiSettings.tokenHeader: apiSettings.tokenPrefix + token]
            print(headers!.description)
         } else {
            // completion(Response())
            print("NO TOKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            return
         }
      }
      
      guard let URL = try? URLRequest(url: urlString, method: .post, headers: headers) else { completion(nil); return }
      
      Alamofire.upload(multipartFormData: { formData in
         
         formData.append(data, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
         
      }, with: URL, encodingCompletion: { encodingResult in
         switch encodingResult {
         case .success(let uploadRequest, _, _):
            
            uploadRequest.validate().responseJSON { responseJSON in
               
               let validResponse = Response(responseJSON)
               
               switch responseJSON.result {
               case .success(let value):
                  print(value)
                  completion(validResponse)
               case .failure(let error):
                  print(error)
                  completion(validResponse)
               }
            }
            
         case .failure(let encodingError):
            completion(nil)
            print("error:\(encodingError)")
         }
      })
   }
}

// MARK: --------- promises extension

public enum ApiPromiseError: Error {
   case
      noInternet,
      noToken,
      microServicesEnumRaw,
      paramsKeyValue,
      createJsonError,
      cancel,
      unknown(Error)
   
   public var localizedDescription: String {
      print(self)
      
      switch self {
      case .noInternet:
         return NSLocalizedString("No Internet", comment: "ApiPromiseError")
      case .noToken:
         return NSLocalizedString("No token", comment: "ApiPromiseError")
      case .microServicesEnumRaw:
         return NSLocalizedString("Error with Microservice raw init", comment: "ApiPromiseError")
      case .paramsKeyValue:
         return NSLocalizedString("Error in request's parameters", comment: "ApiPromiseError")
      case .createJsonError:
         return NSLocalizedString("Cannot create JSON from data", comment: "ApiPromiseError")
      case .unknown(let error):
         return error.localizedDescription
      case .cancel:
         return NSLocalizedString("Cancel", comment: "ApiPromiseError")
      }
   }
}
