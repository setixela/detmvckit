//
//  DataKey.swift
//  Invercity
//
//  Created by AlexSolo on 06.06.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public enum DataKey: String {
   case
   ///register
   accessToken, //string
   refreshToken, //string
   expiresIn, //double
   tokenType //string
   ///auth
   
   ///profile
}
