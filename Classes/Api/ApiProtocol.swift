//
//  ApiProtocol.swift
//  SetixelaMVCKit
//
//  Created by AlexSolo on 26.12.2017.
//  Copyright © 2017 Gaozhiyuan. All rights reserved.
//

///// USAGE /////

///////////////////

import Alamofire
import Foundation
import SetixelaUtils

public enum HttpSheme {
   case
      http,
      https
}

public protocol ApiProtocol: Singleton {
}

public protocol ApiSettingsProtocol {
   var tokenHeader: String { get }
   var tokenPrefix: String { get }
   var httpScheme: HttpSheme { get set }
}
