//
//  ApiExtensions.swift
//  GdeDuet2
//
//  Created by Setixela-vmware on 02.02.17.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation
import UIKit

enum FieldTypes {
   case
   string,
   int,
   bool,
   cllocationDegrees
}

///////////////////////////////////////////////////////////////////////////////////////////
public typealias JSON = [String: Any]
///////////////////////////////////////////////////////////////////////////////////////////
public typealias DictionaryInt = [String: Int]
///////////////////////////////////////////////////////////////////////////////////////////
extension Dictionary: Contentable {
   public subscript () -> Any? {
         return self.first?.value
   }
   
   public var stringValue: String? {
      return self.value as? String
   }
   
   public var intValue: Int? {
      return self.value as? Int
   }
   
   public var key: String {
      return self.first?.key as? String ?? ""
   }
	
   public var value: Any? {
      return self.first?.value ?? nil
   }
}

public protocol JSONModelProtocol {
   
   associatedtype Fields: RawRepresentable
   
   var json: JSON { get set }
   init(json: JSON?)
   
   func string(_ field: Fields) -> String
   func int(_ field: Fields) -> Int
  // func date(_ field: Fields) -> Date
   func bool(_ field: Fields) -> Bool
	func stringBool(_ field: Fields) -> Bool
   
   subscript (field: Fields) -> Any { get set }
   
   func rawString(_ field: Fields) -> String
}

extension JSONModelProtocol {
   
   public func rawString(_ field: Fields) -> String {
      return field.rawValue as? String ?? ""
   }
   
   public func value(field: Fields)->Any? {
      return self.json[rawString(field)] ?? nil
   }
   ///////
   public func string(_ field: Fields) -> String {
      return self.json[rawString(field)] as? String ?? ""
   }
   public func int(_ field: Fields) -> Int {
      return self.json[rawString(field)] as? Int ?? 0
   }
   public func intOptional(_ field: Fields) -> Int? {
      return self.json[rawString(field)] as? Int
   }
   public func float(_ field: Fields) -> CGFloat {
      return self.json[rawString(field)] as? CGFloat ?? 0
   }
   
   public func dictionary(_ field: Fields) -> [String: Any] {
      if let string = self.json[rawString(field)] as? String {
         if let data = string.data(using: .utf8) {
            do {
               return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
            } catch {
               print(error.localizedDescription)
            }
         }
         return [:]
         
      }
      return [:]
   }
   public func intArrayFromString(_ field: Fields) -> [Int] {
      if let str = value(field: field) as? String {
         if str.count>0 {
            let strArray: [Int] = str.components(separatedBy: ",").map { Int($0)! }
            return strArray
         } else {
            return []
         }
      }
      return []
   }
   public func intSetFromString(_ field: Fields) -> Set<Int> {
      if let str = value(field: field) as? String {
         if str.count>0 {
            var strSet: Set<Int> = []
            let arr = str.components(separatedBy: ",").map { Int($0)! }
            for elmnt in arr {
               strSet.insert(elmnt)
            }
            return strSet
         } else {
            return []
         }
      }
      return []
   }
   public func intDictionary(_ field: Fields) -> [String: Int] {
      if let dic = self.json[rawString(field)] as? [String: Int] {
         return dic
      }
      return [:]
   }
   
   public func cgFloatDictionary(_ field: Fields) -> [String: CGFloat] {
      if let dic = self.json[rawString(field)] as? [String: CGFloat] {
         return dic
      }
      return [:]
   }
   
   public func dateInLocale() -> Date {
      return Date(timeIntervalSince1970: 0)
   }
   public func bool(_ field: Fields) -> Bool {
      let intValue = self.int(field)
      if intValue > 0 {
         return true
      }
      return false
   }
	public func stringBool(_ field: Fields) -> Bool {
		if let str = value(field: field) as? String {
			if str == "true" {
				return true
			} else {
				return false
			}
		}
		return false
	}

	public func url(_ field: Fields) -> URL? {
		if let str = self.json[rawString(field)] as? String {
			if let url = URL(string: str) {
				return url
			}
		}
		return nil
	}
	
	public func jsonModel(_ field: Fields) -> JSON? {
		if let val = self.json[rawString(field)] as? JSON {
			return val
		}
		return nil
	}
	
   public func jsonArray(_ field: Fields) -> [JSON]? {
      if let val = self.json[rawString(field)] {
			let array = val as? [JSON]
         return array
      }
      return nil
   }
   
   ///////////////
   public subscript (field: Fields) -> Any {
      get {
         return self.json[rawString(field)] as? String ?? "\n \n \n Error! Nil value in JSONModelProtocol!!!! \n \n \n "
      }
      set {
         self.json[rawString(field)] = newValue
      }
   }
}
