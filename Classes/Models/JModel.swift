//
//  JModel.swift
//  GdeDuet2
//
//  Created by setiXela on 03/05/2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

open class JModel: Contentable {
   public var json: JSON = [:]
   
   public required init(json: JSON?) {
      if json != nil {
         self.json = json!
      }
   }
}

extension CellTypable where Self: JModel {
   public init(json: JSON?, cellType: EnumCells) {
      self.init(json: json)
      
      self.cellType = cellType
   }
}
